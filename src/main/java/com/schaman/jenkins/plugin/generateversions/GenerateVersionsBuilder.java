package com.schaman.jenkins.plugin.generateversions;

import com.cloudbees.plugins.credentials.CredentialsProvider;
import com.cloudbees.plugins.credentials.common.StandardListBoxModel;
import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials;
import com.schaman.jenkins.plugin.globalconfigurations.Project;
import hudson.Extension;
import hudson.FilePath;
import hudson.Launcher;
import hudson.Util;
import hudson.model.*;
import hudson.security.ACL;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.Builder;
import hudson.util.FormValidation;
import hudson.util.ListBoxModel;
import hudson.util.Secret;
import jenkins.model.Jenkins;
import jenkins.tasks.SimpleBuildStep;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.jenkinsci.Symbol;
import org.jenkinsci.plugins.tokenmacro.TokenMacro;
import org.kohsuke.stapler.AncestorInPath;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.verb.POST;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class GenerateVersionsBuilder extends Builder implements SimpleBuildStep {
    private final String generate;
    private final String credentialsId;
    private final long maxWait;
    private final long sleepWait;
    public final List<GenerateVersionsProject> projects;
    private final String minor;

    @DataBoundConstructor
    public GenerateVersionsBuilder(String generate, String credentialsId, String minor, String maxWait, String sleepWait, List<GenerateVersionsProject> projects) {
        this.generate = generate;
        this.credentialsId = credentialsId;
        this.minor = StringUtils.trim(minor);
        this.maxWait = Long.parseLong(StringUtils.trim(maxWait));
        this.sleepWait = Long.parseLong(StringUtils.trim(sleepWait));
        this.projects = projects;
    }

    public String getGenerate() { return generate; }

    public String getCredentialsId() {
        return credentialsId;
    }

    public String getMinor() {
        return minor;
    }

    public long getMaxWait() {
        return maxWait;
    }

    public long getSleepWait() {
        return sleepWait;
    }

    public List<GenerateVersionsProject> getProjects() {
        return projects;
    }

    @Override
    public void perform(Run<?, ?> run, FilePath workspace, Launcher launcher, TaskListener taskListener) throws InterruptedException, IOException {
        try {
            String jobNameEnv = run.getEnvironment(taskListener).get("JOB_NAME");
            String buildDisplayNameEnv = run.getEnvironment(taskListener).get("BUILD_DISPLAY_NAME");
            // TODO : cambiar el canal Y token de Zoom al de Builds&Deployments
//            String hookId = "89zGH4EIFwO2IgNNj79CSHk6"; // Builds&Deployments
//            String token = "wyklciLAcf-uCxIA08BMHXsH"; // Builds&Deployments
            String hookId = "AX6U2_GWH8O_FBerfQq5WADl"; // Sandbox
            String token = "EzG--FSTEFCHobuE58jxexzB"; // Sandbox
            String head = jobNameEnv + " " + buildDisplayNameEnv;
            Zoom zoom = new Zoom(hookId, token, head, "negro", taskListener);

            if (generate == null) {
                throw new RuntimeException("Es necesario que el campo 'Generate' no sea null, selecciona una de las opciones disponibles");
            }

            if (StringUtils.isBlank(credentialsId)) {
                throw new RuntimeException("Es necesario seleccionar credenciales, no pueden ser none");
            }
            StandardUsernamePasswordCredentials c = CredentialsProvider.findCredentialById(credentialsId, StandardUsernamePasswordCredentials.class, run, Collections.emptyList());
            String username = c != null ? c.getUsername() : null;
            if (username == null) {
                throw new RuntimeException("No se ha podido obtener el username de las credenciales");
            }
            Secret password = c.getPassword();
            String passwordStr = password.getPlainText();
            final org.eclipse.jgit.transport.CredentialsProvider CREDENTIALS_PROVIDER = new UsernamePasswordCredentialsProvider(username, passwordStr);

            if (minor.isEmpty()) {
                throw new RuntimeException("Minor version no puede estar vacío");
            }
            String minorValue = TokenMacro.expandAll((AbstractBuild<?, ?>) run, taskListener, this.minor);
            if (minorValue.isEmpty()) {
                throw new RuntimeException("Minor version no tiene ningún valor");
            }
            if (!isNumeric(minorValue)) {
                throw new RuntimeException("Minor version solo puede tener números");
            }

            Set<Project> globalProjects = Utils.getProjectsSchaman();
            List<GenerateVersionsProject> compiledProjects = new ArrayList<>();

            // Comprobar que todos los proyectos de la tarea tienen nombre
            checkProjectName(this.projects);

            // Comprobar que los proyectos existen en la configuración global
            checkProjectsInGlobalConfig(this.projects, globalProjects);

            // Comprobar que todos los proyectos tienen configurado el artifacId y el jobName en la configuración global
            checkProjectsWithArtifactIdAndJobName(this.projects, globalProjects);

            // Notificar por Zoom los proyectos que se van a compilar
            String projectNames = getProjectNames(this.projects);
            String msgZoom = "Se va a generar versiones " + generate + " de los siguientes proyectos:\n" + projectNames;
            Utils.log(taskListener, msgZoom);
            zoom.setBody(msgZoom);
            // TODO : DESCOMENTAR NOTIFICAR POR ZOOM
//            zoom.notifyZoom();

            if ("snapshot".equalsIgnoreCase(generate)) {
                Utils.log(taskListener, "generate: " + generate);
                Snapshot.generateVersions(run, workspace, taskListener, CREDENTIALS_PROVIDER, minorValue, maxWait, sleepWait, projects, compiledProjects, globalProjects, zoom);
            } else if ("rc".equalsIgnoreCase(generate)) {
                Utils.log(taskListener, "generate: " + generate);
                Rc.generateVersions(workspace, taskListener, CREDENTIALS_PROVIDER, minorValue, maxWait, sleepWait, projects, compiledProjects, globalProjects, zoom);
            } else if ("release".equalsIgnoreCase(generate)) {
                Utils.log(taskListener, "generate: " + generate);
                Release.generateVersions(workspace, taskListener, CREDENTIALS_PROVIDER, minorValue, maxWait, sleepWait, projects, compiledProjects, globalProjects, zoom);
            }

        } catch (Throwable e) {
            AbstractBuild<?, ?> build = (AbstractBuild<?, ?>) run;
            GenerateEnvVar errorEnvVar = new GenerateEnvVar("ERROR_MESSAGE", "[Generate Versions] " + e.getMessage());
            build.addAction(errorEnvVar);
//            Utils.log(taskListener, "Se ha creado la variable de entorno " + errorEnvVar.getVarKey());
            Utils.log(taskListener, "Se ha creado la variable de entorno " + errorEnvVar.getVarKey() + "\n" + errorEnvVar.getVarValue());
            throw new RuntimeException("[Generate Versions] " + e.getMessage());
        } finally {
            // Crea variable con el nombre de los proyectos y su estado para notificar por Zoom
            String msg = "Resultado global" + getProjectStatus(this.projects);
            AbstractBuild<?, ?> build = (AbstractBuild<?, ?>) run;
            GenerateEnvVar globalResultEnvVar = new GenerateEnvVar("GLOBAL_RESULT", msg);
            build.addAction(globalResultEnvVar);
//            Utils.log(taskListener, "Se ha creado la variable de entorno " + globalResultEnvVar.getVarKey());
            Utils.log(taskListener, "Se ha creado la variable de entorno " + globalResultEnvVar.getVarKey() + "\n" + globalResultEnvVar.getVarValue());
        }
    }

    protected static boolean isNumeric(String minorValue) {
        try {
            Integer.parseInt(minorValue);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    protected static void checkProjectName(List<GenerateVersionsProject> projects) {
        for (GenerateVersionsProject project : projects) {
            String projectName = project.getProjectName();
            if (StringUtils.isBlank(projectName)) {
                throw new RuntimeException("No puede haber ningún proyecto con el nombre vacío en la configuración de la tarea");
            }
        }
    }

    protected static void checkProjectsInGlobalConfig(List<GenerateVersionsProject> jobProjects, Set<Project> globalProjects) {
        List<String> nonConfiguredProjects = new ArrayList<>();
        for (GenerateVersionsProject project : jobProjects) {
            String projectName = project.getProjectName().trim();
            Project globalProject = Utils.getProjectSchamanByProjectName(globalProjects, projectName);
            if (globalProject == null) {
                nonConfiguredProjects.add(projectName);
            }
        }

        if (nonConfiguredProjects.size() > 0) {
            if (nonConfiguredProjects.size() == 1) {
                throw new RuntimeException("El proyecto " + nonConfiguredProjects.get(0) + " no está en la configuración global, porque se ha eliminado o no se ha añadido");
            } else {
                StringBuilder msg = new StringBuilder("Los siguientes proyectos no están en la configuración global, porque se han eliminado o no se han añadido:\n");
                for (String nonConfiguredProject : nonConfiguredProjects) {
                    msg.append(nonConfiguredProject).append("\n");
                }
                throw new RuntimeException(msg.toString());
            }
        }
    }

    protected static void checkProjectsWithArtifactIdAndJobName(List<GenerateVersionsProject> jobProjects, Set<Project> globalProjects) {
        List<String> nonConfiguredGlobalProjects = new ArrayList<>();
        for (GenerateVersionsProject project : jobProjects) {
            String projectName = project.getProjectName().trim();
            Project globalProject = Utils.getProjectSchamanByProjectName(globalProjects, projectName);
            if (globalProject != null) {
                String artifactId = globalProject.getArtifactId().trim();
                String jobName = globalProject.getJobName().trim();
                if (StringUtils.isBlank(artifactId) && StringUtils.isBlank(jobName)) {
                    String msg = "El proyecto " + projectName + " no tiene Artifact Id, ni Job Name en la configuración global.";
                    nonConfiguredGlobalProjects.add(msg);
                } else if (StringUtils.isBlank(artifactId)) {
                    String msg = "El proyecto " + projectName + " no tiene Artifact Id en la configuración global.";
                    nonConfiguredGlobalProjects.add(msg);
                } else if (StringUtils.isBlank(jobName)) {
                    String msg = "El proyecto " + projectName + " no tiene Job Name en la configuración global.";
                    nonConfiguredGlobalProjects.add(msg);
                }
            }
        }
        if (nonConfiguredGlobalProjects.size() > 0) {
            StringBuilder msg = new StringBuilder();
            for (String nonConfiguredGlobalProject : nonConfiguredGlobalProjects) {
                msg.append(nonConfiguredGlobalProject).append("\n");
            }
            throw new RuntimeException(msg.toString());
        }
    }

    protected static String getProjectNames(List<GenerateVersionsProject> projects) {
        StringBuilder projectsName = new StringBuilder();
        for (GenerateVersionsProject project : projects) {
            projectsName.append("- ").append(project.getProjectName()).append("\n");
        }
        return projectsName.toString();
    }

    protected static String getProjectStatus(List<GenerateVersionsProject> projects) {
        StringBuilder msg = new StringBuilder("Resultado global");
        for (GenerateVersionsProject project : projects) {
            msg.append("\n- ").append(project.getProjectName()).append(" : ").append(project.getStatus());
        }
        return msg.toString();
    }

    @Symbol("generateVersionsBuilder")
    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {
        public DescriptorImpl() {
            super(GenerateVersionsBuilder.class);
            load();
        }

        public ListBoxModel doFillGenerateItems(@AncestorInPath final Item item, @QueryParameter final String generate) {
            String[] generateItems = {"SNAPSHOT", "RC", "RELEASE"};
            ListBoxModel items = new ListBoxModel();

            for (String generateItem : generateItems) {
                items.add(generateItem);
            }

            return items;
        }

        public ListBoxModel doFillCredentialsIdItems(@AncestorInPath final Item item, @QueryParameter final String credentialsId) {
            StandardListBoxModel result = new StandardListBoxModel();
            if (item == null) {
                Jenkins instance = Jenkins.get();
                if (!instance.hasPermission(Jenkins.ADMINISTER)) {
                    return result.includeCurrentValue(credentialsId);
                }
            } else {
                if (!item.hasPermission(Item.EXTENDED_READ)
                        && !item.hasPermission(CredentialsProvider.USE_ITEM)) {
                    return result.includeCurrentValue(credentialsId);
                }
            }
            return result.includeAs(
                    ACL.SYSTEM,
                    item,
                    StandardUsernamePasswordCredentials.class,
                    Collections.emptyList())
                    .includeCurrentValue(credentialsId);
        }

        @POST
        public FormValidation doCheckCredentialsId(@AncestorInPath Item item, @QueryParameter String credentialsId) {
            if (item == null) {
                if (!Jenkins.get().hasPermission(Jenkins.ADMINISTER)) {
                    return FormValidation.ok();
                }
            } else {
                if (!item.hasPermission(Item.EXTENDED_READ)
                        && !item.hasPermission(CredentialsProvider.USE_ITEM)) {
                    return FormValidation.ok();
                }
            }
            if (StringUtils.isBlank(credentialsId)) {
                return FormValidation.ok();
            }
            if (credentialsId.startsWith("${") && credentialsId.endsWith("}")) {
                return FormValidation.warning("Cannot validate expression based credentials");
            }
            return FormValidation.ok();
        }

        @POST
        public FormValidation doCheckMinor(@QueryParameter String minor, @AncestorInPath Item item) {
            if (item == null) { // no context
                return FormValidation.ok();
            }
            item.checkPermission(Item.CONFIGURE);

            if (Util.fixEmptyAndTrim(minor) == null) {
                return FormValidation.error("Minor version no puede estar vacío");
            }
            return FormValidation.ok();
        }

        @POST
        public FormValidation doCheckMaxWait(@QueryParameter String maxWait, @AncestorInPath Item item) {
            if (item == null) { // no context
                return FormValidation.ok();
            }
            item.checkPermission(Item.CONFIGURE);

            if (Util.fixEmptyAndTrim(maxWait) == null) {
                return FormValidation.error("Project compile timeout (ms) no puede estar vacío");
            }
            return FormValidation.ok();
        }

        @POST
        public FormValidation doCheckSleepWait(@QueryParameter String sleepWait, @AncestorInPath Item item) {
            if (item == null) { // no context
                return FormValidation.ok();
            }
            item.checkPermission(Item.CONFIGURE);

            if (Util.fixEmptyAndTrim(sleepWait) == null) {
                return FormValidation.error("Project check status Interval (ms) no puede estar vacío");
            }
            return FormValidation.ok();
        }

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> jobType) {
            return true;
        }

        @Override
        public String getDisplayName() {
            return "Generate Versions";
        }
    }
}
