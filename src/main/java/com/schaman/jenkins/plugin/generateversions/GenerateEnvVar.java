package com.schaman.jenkins.plugin.generateversions;

import hudson.EnvVars;
import hudson.model.AbstractBuild;
import hudson.model.EnvironmentContributingAction;
import hudson.model.InvisibleAction;
import hudson.model.Run;

import javax.annotation.Nonnull;

public class GenerateEnvVar extends InvisibleAction implements EnvironmentContributingAction {
    private final @Nonnull
    String varKey;
    private final @Nonnull
    String varValue;

    public GenerateEnvVar(@Nonnull String varKey, @Nonnull String varValue) {
        this.varKey = varKey;
        this.varValue = varValue;
    }

    @Nonnull
    public String getVarKey() {
        return varKey;
    }

    @Nonnull
    public String getVarValue() {
        return varValue;
    }

    @Override
    public void buildEnvironment(@Nonnull Run<?, ?> run, @Nonnull EnvVars env) {
        env.put(varKey, varValue);
    }

    @Override
    public void buildEnvVars(AbstractBuild<?, ?> abstractBuild, EnvVars envVars) {
        envVars.put(varKey, varValue);
    }
}
