package com.schaman.jenkins.plugin.generateversions;

import com.google.gson.Gson;
import hudson.model.TaskListener;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Zoom {
    private String hookId;
    private String token;
    private String head;
    private String body;
    private String color;
    private TaskListener taskListener;

    public Zoom(String hookId, String token, String head, String body, String color, TaskListener taskListener) {
        this.hookId = hookId;
        this.token = token;
        this.head = head;
        this.body = body;
        this.color = color;
        this.taskListener = taskListener;
    }

    public Zoom(String hookId, String token, String head, String color, TaskListener taskListener) {
        this.hookId = hookId;
        this.token = token;
        this.head = head;
        this.body = null;
        this.color = color;
        this.taskListener = taskListener;
    }

    protected String getHookId() {
        return hookId;
    }

    protected void setHookId(String hookId) {
        this.hookId = hookId;
    }

    protected String getToken() {
        return token;
    }

    protected void setToken(String token) {
        this.token = token;
    }

    protected String getHead() {
        return head;
    }

    protected void setHead(String head) {
        this.head = head;
    }

    protected String getBody() {
        return body;
    }

    protected void setBody(String body) {
        this.body = body;
    }

    protected String getColor() {
        return color;
    }

    protected void setColor(String color) {
        this.color = color;
    }

    protected TaskListener getTaskListener() {
        return taskListener;
    }

    protected void setTaskListener(TaskListener taskListener) {
        this.taskListener = taskListener;
    }

    protected void notifyZoom() {
        HttpURLConnection connection = null;
        try {
            if (color == null) {
                setColor("Negro");
            }

            String hexColor = getHexadecimalColor(color);

            String resolvedHook = hookId;
            String resolvedAuthorization = token;
            String resolvedHead = head;
            String resolvedBody = body;
            String rawUrl = "https://inbots.zoom.us/incoming/hook/" + resolvedHook + "?format=full";

            resolvedHead = StringUtils.isBlank(resolvedHead) ? " " : resolvedHead;
            resolvedBody = StringUtils.isBlank(resolvedBody) ? " " : resolvedBody;

            Map<String, Object> requestData = new HashMap<>();

            Map<String, Object> styleHead = new HashMap<>();
            styleHead.put("color", hexColor);
            styleHead.put("bold", true);
            styleHead.put("italic", false);

            Map<String, Object> requestDataHead = new HashMap<>();
            requestDataHead.put("type", "message");
            requestDataHead.put("text", resolvedHead);
            requestDataHead.put("style", styleHead);
            requestData.put("head", requestDataHead);

            List<Map<String, Object>> sectionsBody = new ArrayList<>();
            Map<String, Object> sectionBody = new HashMap<>();
            sectionBody.put("type", "message");
            sectionBody.put("text", resolvedBody);
            sectionsBody.add(sectionBody);

            Map<String, Object> requestDataBody = new HashMap<>();
            requestDataBody.put("type", "section");
            requestDataBody.put("sidebar_color", hexColor);
            requestDataBody.put("sections", sectionsBody);
            requestData.put("body", Collections.singletonList(requestDataBody));

            String jsonRequestBody = new Gson().toJson(requestData);

            taskListener.getLogger().println("INFO - Jenkins Zoom - resolvedHook: " + resolvedHook);
            taskListener.getLogger().println("INFO - Jenkins Zoom - resolvedAuthorization: " + resolvedAuthorization);
            taskListener.getLogger().println("INFO - Jenkins Zoom - resolvedHead: " + resolvedHead);
            taskListener.getLogger().println("INFO - Jenkins Zoom - resolvedBody: " + resolvedBody);
            taskListener.getLogger().println("INFO - Jenkins Zoom - url: " + rawUrl);
            taskListener.getLogger().println("INFO - Jenkins Zoom - request: " + jsonRequestBody);

            URL url = new URL(rawUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", resolvedAuthorization);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = jsonRequestBody.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;

                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }

                taskListener.getLogger().println("INFO - Jenkins Zoom - response: " + response.toString());
            }

        } catch (Throwable e) {
            taskListener.getLogger().println("ERROR - Jenkins Zoom - error message: " + e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    protected String getHexadecimalColor(String color) {
        if (color.equalsIgnoreCase("verde")) {
            return "#04B404";
        } else if (color.equalsIgnoreCase("rojo")) {
            return "#DF0101";
        } else if (color.equalsIgnoreCase("naranja")) {
            return "#DF7401";
        } else {
            return "#000000";
        }
    }
}

