package com.schaman.jenkins.plugin.generateversions;

public class UpdatedSchamanDependencies {
    protected boolean updated;
    protected String dependencies;

    public UpdatedSchamanDependencies() {
        this.updated = false;
        this.dependencies = null;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public String getDependencies() {
        return dependencies;
    }

    public void setDependencies(String dependencies) {
        this.dependencies = dependencies;
    }
}
