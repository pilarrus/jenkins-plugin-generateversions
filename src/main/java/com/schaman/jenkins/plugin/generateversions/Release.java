package com.schaman.jenkins.plugin.generateversions;

import com.schaman.jenkins.plugin.globalconfigurations.Project;
import com.ximpleware.*;
import hudson.FilePath;
import hudson.model.Job;
import hudson.model.TaskListener;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RefSpec;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Release {
    protected static void generateVersions(
            FilePath workspace,
            TaskListener taskListener,
            org.eclipse.jgit.transport.CredentialsProvider credentialsProvider,
            String minorValue,
            long maxWait,
            long sleepWait,
            List<GenerateVersionsProject> projects,
            List<GenerateVersionsProject> compiledProjects,
            Set<Project> globalProjects,
            Zoom zoom
    ) throws IOException, GitAPIException {
        for (GenerateVersionsProject project : projects) {
            Git git = null;
            String fullPath = null;
            String nameDirClone = UUID.randomUUID().toString();
            String dirClone = workspace.getRemote() + File.separator + nameDirClone + File.separator;
            try {
                String projectName = project.getProjectName();
                if (StringUtils.isBlank(projectName)) {
                    throw new RuntimeException("El nombre del proyecto no puede estar vacío");
                }
                Utils.log(taskListener, "Nombre del proyecto: " + projectName);

                // Notificar por Zoom que comienzan las operaciones necesarias para generar release
                String zoomMsg = "Comienzan las operaciones necesarias para generar la versión RELEASE del proyecto " + projectName;
                Utils.log(taskListener, zoomMsg);
                zoom.setBody(zoomMsg);
                // TODO : Descomentar notificar por Zoom
//                zoom.notifyZoom();

                fullPath = dirClone + projectName;
                git = Utils.cloneRepository(projectName, dirClone, credentialsProvider);
                Repository repository = git.getRepository();

                /* Paso 1 : Borrar todas las tags que tienen minor igual al actual minor y sean SNAPSHOT */
                List<String> tags = Utils.getTags(git);
                List<String> tagsToDelete = Utils.getTagsWithSameMinorAndSnapshot(tags, minorValue);
                if (tagsToDelete.size() > 0) {
                    Utils.log(taskListener, "Nº de tags antes de borrar las tags que sean *." + minorValue + ".*-SNAPSHOT: " + tags.size());
                    Utils.log(taskListener, "Tags para borrar: " + tagsToDelete.toString());
                    Utils.deleteTags(git, tagsToDelete, credentialsProvider);
                    tags = Utils.getTags(git);
                    Utils.log(taskListener, "Nº de tags después de borrar: " + tags.size());
                } else {
                    Utils.log(taskListener, "No hay tags *." + minorValue + ".*-SNAPSHOT para borrar");
                }

                /* Paso 2 : ¿Existe la rama v12? */
                String branch = "v" + minorValue;
                boolean existsBranch = Utils.existsBranch(git, branch, credentialsProvider);
                if (!existsBranch) {
                    /* Paso 2.2 */
                    Utils.log(taskListener, "No existe la rama " + branch);
                    /* Paso 2.2.1.1 : Borrar todas las tags que tienen minor igual al actual minor y sean RC */
                    tagsToDelete = Utils.getTagsWithSameMinorAndRc(tags, minorValue);
                    if (tagsToDelete.size() > 0) {
                        Utils.log(taskListener, "Nº de tags antes de borrar las tags que sean *." + minorValue + ".*-RC: " + tags.size());
                        Utils.log(taskListener, "Tags para borrar: " + tagsToDelete.toString());
                        Utils.deleteTags(git, tagsToDelete, credentialsProvider);
                        tags = Utils.getTags(git);
                        Utils.log(taskListener, "Nº de tags después de borrar: " + tags.size());
                    } else {
                        Utils.log(taskListener, "No hay tags *." + minorValue + ".*-RC para borrar");
                    }
                    continue;
                }

                /* Paso 2.1 : Me sitúo en v12 */
                Utils.checkout(git, branch);
                String currentBranch = repository.getFullBranch();
                if (!currentBranch.contains(branch)) {
                    throw new RuntimeException("Git no se ha podido cambiar a la rama " + branch + " sigue en master del proyecto " + projectName);
                }
                Utils.log(taskListener, "Git se ha cambiado a la rama: " + branch);

                /* Paso 3 : Obtener la versión actual del pom.xml */
                File pomFile = Utils.getFile(repository, "pom.xml");
                String versionPom = null;
                if (pomFile != null) {
                    versionPom = Utils.getVersionPom(pomFile);
                    if (versionPom == null) {
                        throw new RuntimeException("No se ha podido obtener la versión del pom.xml del proyecto " + projectName);
                    }
                    Utils.log(taskListener, "Versión obtenida del pom.xml: " + versionPom);
                }

                /* Paso 3 : Obtener la versión actual del package.json */
                File packageFile = Utils.getFile(repository, "package.json");
                String versionPackage = null;
                if (packageFile != null) {
                    versionPackage = Utils.getVersionPackage(packageFile);
                    if (versionPackage == null) {
                        throw new RuntimeException("No se ha podido obtener la versión del package.json");
                    }

                    Utils.log(taskListener, "Versión obtenida del package.json: " + versionPackage);
                }

                if (pomFile == null && packageFile == null) {
                    throw new RuntimeException("El proyecto " + projectName + " no tiene pom.xml ni package.json");
                }

                /*MERGE development -> v13 */
                Utils.mergeProcess(git, taskListener, branch, credentialsProvider, pomFile, versionPom, packageFile, versionPackage);
                /*FIN MERGE */

                String commitMessage = "";
                boolean updatedVersionPom = false;
                /* Paso 5 : Poner la versión del pom.xml en formato release */
                if (versionPom != null && (versionPom.contains("-SNAPSHOT") || versionPom.contains("-RC"))) {
                    String[] split = versionPom.split("-");
                    String versionRelease = split[0];
                    updatedVersionPom = Utils.updateVersionPom(pomFile, versionRelease);
                    versionPom = Utils.getVersionPom(pomFile);
                    if ((versionPom != null && !versionPom.equalsIgnoreCase(versionRelease))) {
                        throw new RuntimeException("No se ha podido cambiar la versión del pom.xml a formato release");
                    }
                }

                if (updatedVersionPom) {
                    String msg = "Se ha cambiado la versión en el pom.xml a " + versionPom + ". ";
                    commitMessage += msg;
                    Utils.log(taskListener, msg);
                }

                boolean updatedVersionPackage = false;
                /* Paso 5 : Poner la versión del package.json en formato release */
                if (versionPackage != null && (versionPackage.contains("-SNAPSHOT") || versionPackage.contains("-RC"))) {
                    String[] split = versionPackage.split("-");
                    String versionRelease = split[0];
                    updatedVersionPackage = Utils.updateVersionPackage(packageFile, versionRelease);
                    versionPackage = Utils.getVersionPackage(packageFile);
                    if ((versionPackage != null && !versionPackage.equalsIgnoreCase(versionRelease))) {
                        throw new RuntimeException("No se ha podido cambiar la versión del package.json a formato release");
                    }
                }

                if (updatedVersionPackage) {
                    String msg = "Se ha cambiado la versión en el package.json a " + versionPackage + ". ";
                    commitMessage += msg;
                    Utils.log(taskListener, msg);
                }

//                /* Paso 6 : Leer dependencias de Schaman del pom.xml y actualizar su versión si es necesario */
//                boolean updatedDependenciesPom = false;
//                UpdatedSchamanDependencies updatedSchamanDependencies = null;
//                List<File> pomFiles = Utils.getPoms(repository, taskListener);
//                for (File pomXml : pomFiles) {
//                    updatedSchamanDependencies = readAndUpdateSchamanDependencies(pomXml, compiledProjects, globalProjects, dirClone, credentialsProvider, minorValue, projectName, taskListener);
//                    boolean anyUpdatedDependency = updatedSchamanDependencies.isUpdated();
//                    if (anyUpdatedDependency) {
//                        updatedDependenciesPom = true;
//                    }
//                }
                /* Paso 6 : Leer dependencias schaman de los ficheros pom.xml y actualizar su versión si es necesario */
                boolean updatedDependenciesPom = false;
                // TODO : Comprobar que updatedSchamanDependencies no se sobrescribe en cada iteración
                UpdatedSchamanDependencies updatedSchamanDependencies = new UpdatedSchamanDependencies();
                List<File> pomFiles = Utils.getPoms(repository, taskListener);
                for (File pomXml : pomFiles) {
                    readAndUpdateVersionDependenciesPom(pomXml, updatedSchamanDependencies, compiledProjects, globalProjects, dirClone, credentialsProvider, minorValue, projectName, taskListener);
                    boolean anyUpdatedDependency = updatedSchamanDependencies.isUpdated();
                    if (anyUpdatedDependency) {
                        updatedDependenciesPom = true;
                    }
                }

                if (pomFile != null) {
                    Utils.log(taskListener, "Se han actualizado las dependencias del pom.xml: " + updatedDependenciesPom);
                }

                if (updatedDependenciesPom) {
                    String msg = "Se ha actualizado la versión de las dependencias del pom.xml " + updatedSchamanDependencies.getDependencies();
                    commitMessage += msg;
                    Utils.log(taskListener, msg);
                }

                /* Paso 6 : Leer dependencias de Schaman del package.json y actualizar su versión si es necesario */
                boolean updatedDependenciesPackage = false;
                UpdatedSchamanDependencies updatedSchamanDependenciesPackage = null;
                if (packageFile != null) {
                    updatedSchamanDependenciesPackage = readAndUpdateVersionDependencyPackage(packageFile, compiledProjects, dirClone, credentialsProvider, minorValue, projectName, taskListener);
                    boolean anyUpdatedDependency = updatedSchamanDependenciesPackage.isUpdated();
                    if (anyUpdatedDependency) {
                        updatedDependenciesPackage = true;
                    }
                }

                if (packageFile != null) {
                    Utils.log(taskListener, "Se han actualizado las dependencias del package.json: " + updatedDependenciesPackage);
                }

                if (updatedDependenciesPackage) {
                    String msg = "Se ha actualizado la versión de las dependencias del package.json " + updatedSchamanDependenciesPackage.getDependencies();
                    commitMessage += msg;
                    Utils.log(taskListener, msg);
                }

                if (updatedVersionPom || updatedVersionPackage || updatedDependenciesPom || updatedDependenciesPackage) {
                    Utils.add(git);
                    Utils.commit(git, commitMessage);
                    Utils.push(git, branch, credentialsProvider);
                    Utils.log(taskListener, "Se ha hecho commit + push de los cambios en la rama " + branch);
                }

                /* MERGE v13 -> development */
                mergeProcessNotAdmitConflicts(git, taskListener, "development", branch, credentialsProvider);
                /*FIN MERGE */

                /* MERGE master -> development */
                mergeProcessNotAdmitConflicts(git, taskListener, "development", "master", credentialsProvider);
                /*FIN MERGE */

                /*MERGE development -> master */
                mergeProcessNotAdmitConflicts(git, taskListener, "master", "development", credentialsProvider);
                /*FIN MERGE */

                Utils.push(git, "master", credentialsProvider);
                Utils.log(taskListener, "Se ha hecho commit + push en la rama master");

                /* Paso 8 : Crear git tag en formato release */
                long startDate = System.currentTimeMillis();
                String version = versionPom != null ? versionPom : versionPackage;
                boolean isTagCreated = Utils.createTag(git, version, credentialsProvider);
                if (!isTagCreated) {
                    throw new RuntimeException("No se ha podido crear la tag " + version + " en el repositorio remoto del proyecto " + projectName);
                }
                Utils.log(taskListener, "Se ha creado la tag: " + version);

                /* Paso 9 : Se espera hasta conocer el estado de compilación de la tarea */
                Project projectSchaman = Utils.getProjectSchamanByProjectName(globalProjects, projectName);
                if (projectSchaman == null) {
                    throw new RuntimeException("No se ha podido encontrar el proyecto " + projectName + " en la lista de proyectos de Schaman");
                }
                String jobName = projectSchaman.getJobName();

                Job<?, ?> job = Utils.getJob(jobName, taskListener);
                if (job == null) {
                    throw new RuntimeException("No existe una tarea en Jenkins con el nombre " + jobName);
                }

                boolean compiled = Utils.isJobCompiled(job, maxWait, startDate, taskListener, sleepWait);

                /* 9.2 : No, fin de la ejecución */
                if (!compiled) {
                    throw new RuntimeException("Tiempo máximo excedido, no se ha compilado la tarea " + jobName);
                }

                /* 9.1 : Borrar tags RC y rama v12 */
                tags = Utils.getTags(git);
                List<String> rcTags = getRcTags(tags);
                if (rcTags.size() > 0) {
                    Utils.log(taskListener, "Nº de tags antes de borrar las que sean RC: " + tags.size());
                    Utils.log(taskListener, "Tags para borrar: " + rcTags.toString());
                    Utils.deleteTags(git, rcTags, credentialsProvider);
                    tags = Utils.getTags(git);
                    Utils.log(taskListener, "Nº de tags después de borrar: " + tags.size());
                } else {
                    Utils.log(taskListener, "No hay tags RC para borrar");
                }

                boolean deleteBranch = deleteBranch(git, branch, credentialsProvider);
                if (deleteBranch) {
                    Utils.log(taskListener, "Se ha borrado la rama " + branch);
                } else {
                    Utils.log(taskListener, "No se ha podido borrar la rama " + branch);
                }

                /* Paso 10 : Se añade proyecto a la lista de compilados */
                project.setVersion(version);
                project.setStatus("OK");
                compiledProjects.add(project);
                Utils.log(taskListener, "Se ha añadido " + projectName + " a proyectos compilados");

            } catch (Throwable e) {
                project.setStatus("KO");
                throw e;
            } finally {
                if (git != null) {
                    git.getRepository().close();
                    git.close();
                }

                if (fullPath != null) {
                    Utils.deleteClonedDirectory(fullPath, taskListener);
                    Utils.deleteClonedDirectory(dirClone, taskListener);
                }

                Utils.log(taskListener, "-----------------------------------------------------");
            }
        }
    }

    protected static void readAndUpdateVersionDependenciesPom(
            File pomFile,
            UpdatedSchamanDependencies updatedSchamanDependencies,
            List<GenerateVersionsProject> compiledProjects,
            Set<Project> globalProjects,
            String dirClone,
            org.eclipse.jgit.transport.CredentialsProvider credentials,
            String minorVersion,
            String parentProjectName,
            TaskListener tasklistener
    ) {
//        UpdatedSchamanDependencies updatedSchamanDependencies = new UpdatedSchamanDependencies();
        StringBuilder updatedDependencies = new StringBuilder();
        String pomPath = pomFile.getAbsolutePath();
        try {
            int i;
            VTDGen vg = new VTDGen();
            if (!vg.parseFile(pomPath, true)) {
                throw new RuntimeException("No se ha podido parsear el pom.xml");
            }
            VTDNav vn = vg.getNav();
            XMLModifier xm = new XMLModifier(vn);
            AutoPilot ap = new AutoPilot(vn);

            ap.selectXPath("/project/dependencies/dependency");
            while ((i = ap.evalXPath()) != -1) {
                if (vn.toElement(VTDNav.FIRST_CHILD, "groupId")) {
                    String groupId = Utils.toNormalizedStringText(vn);

                    if ("schaman".equalsIgnoreCase(groupId) || "schaman-hdm".equalsIgnoreCase(groupId)) {
                        if (vn.toElement(VTDNav.NEXT_SIBLING, "artifactId")) {
                            String artifactId = Utils.toNormalizedStringText(vn);

                            if (vn.toElement(VTDNav.NEXT_SIBLING, "version")) {
                                String versionDependency = Utils.toNormalizedStringText(vn);

                                Project projectSchaman = Utils.getProjectSchamanByArtifactId(globalProjects, artifactId);
                                if (projectSchaman == null) {
                                    throw new RuntimeException("No se ha podido encontrar el proyecto con el artifactID " + artifactId);
                                }

                                String projectName = projectSchaman.getProjectName();
//                                String version = "0.0.0";
                                String version;
                                /* Paso 5 : Comprobar si ha sido compilado */
                                if (Utils.isCompiled(compiledProjects, projectName)) {
                                    /* Paso 5a : Si ha sido compilado cojo la versión del proyecto compilado */
                                    version = Utils.getCompiledVersion(compiledProjects, projectName);
                                    if (version == null) {
                                        throw new RuntimeException("No se ha podido encontrar la version en el proyecto compilado");
                                    }
                                } else {
                                    /* Paso 5b : Sino obtengo la mayor de todas las tags del repo (que no tenga el 2º dígito mayor a minor) */
                                    version = getBiggestVersion(projectName, dirClone, credentials, minorVersion, tasklistener);
                                }
                                int secondDigitVersionDependency = Utils.getSecondDigit(versionDependency);
                                // Si version es mayor que versionDependency o el segundo dígito es mayor que minor actualizar en el pom
                                if (Utils.isGreaterThan(version, versionDependency) || (secondDigitVersionDependency > Integer.parseInt(minorVersion) && !"0.0.0".equalsIgnoreCase(version))) {
                                    // cambiar versión
                                    int j = vn.getText();
                                    xm.updateToken(j, version);
                                    // guardar cambios en el pom
                                    xm.output(pomPath);
                                    updatedSchamanDependencies.setUpdated(true);
                                    updatedDependencies.append(artifactId).append(":").append(version).append(", ");
                                } else {
                                    // Comprobar que la versión de la dependencia schaman no sea RC o SNAPSHOT y que el 2º dígito sea menor o igual (pete)
                                    if (versionDependency.contains("SNAPSHOT")) {
                                        throw new RuntimeException("La dependencia " + artifactId + " del proyecto " + parentProjectName + " contiene SNAPSHOT : " + versionDependency);
                                    }

                                    if (versionDependency.contains("RC")) {
                                        throw new RuntimeException("La dependencia " + artifactId + " del proyecto " + parentProjectName + " contiene RC : " + versionDependency);
                                    }

                                    if (secondDigitVersionDependency > Integer.parseInt(minorVersion) && "0.0.0".equalsIgnoreCase(version)) {
                                        throw new RuntimeException("La versión " + versionDependency + " de la dependencia " + artifactId + " del proyecto " + parentProjectName + " tiene el 2º dígito mayor a " + minorVersion);
                                    }
                                }
                            }
                        }
                    }
                }
                vn.toElement(VTDNav.PARENT);
            }

            if (updatedDependencies.length() > 0) {
                updatedDependencies = new StringBuilder(updatedDependencies.toString().replaceAll(", $", ". "));
                updatedSchamanDependencies.setDependencies(updatedDependencies.toString());
            }
//            return updatedSchamanDependencies;

        } catch (NavException e) {
            throw new RuntimeException("Error NavException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (XPathParseException e) {
            throw new RuntimeException("Error XPathParseException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (XPathEvalException e) {
            throw new RuntimeException("Error XPathEvalException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (ModifyException e) {
            throw new RuntimeException("Error ModifyException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error IOException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (TranscodeException e) {
            throw new RuntimeException("Error TranscodeException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        }
    }

    protected static UpdatedSchamanDependencies readAndUpdateVersionDependencyPackage(
            File file,
            List<GenerateVersionsProject> compiledProjects,
            String dirClone,
            org.eclipse.jgit.transport.CredentialsProvider credentials,
            String minorVersion,
            String parentProject,
            TaskListener tasklistener
    ) {
        UpdatedSchamanDependencies updatedSchamanDependencies = new UpdatedSchamanDependencies();
        StringBuilder updatedDependencies = new StringBuilder();
        try {
            List<String> newLines = new ArrayList<>();
            List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()));

            for (String line : lines) {
                String lineWithoutSpaces = line.replaceAll("\\s", "");
                if (lineWithoutSpaces.trim().startsWith("\"@schaman/") && line.contains("@schaman/")) {
                    lineWithoutSpaces = lineWithoutSpaces.replaceAll("\"", "").replaceAll(",", "");
                    String[] split1 = lineWithoutSpaces.split("/");
                    if (split1.length > 0) {
//                        String groupId = split1[0];
                        String dependencyAndVersion = split1[1];
                        String[] split2 = dependencyAndVersion.split(":");
                        if (split2.length > 0) {
                            String dependencyName = split2[0];
                            String dependencyVersion = split2[1].replace("^", "");

//                            String version = "0.0.0";
                            String version;
                            // Compruebo si es un proyecto compilado
                            boolean isCompiled = Utils.isCompiled(compiledProjects, dependencyName);
                            if (isCompiled) {
                                /* Paso 5a : Si ha sido compilado cojo la versión del proyecto compilado */
                                version = Utils.getCompiledVersion(compiledProjects, dependencyName);
                                if (version == null) {
                                    throw new RuntimeException("No se ha podido encontrar la version en el proyecto compilado");
                                }
                            } else {
                                /* Paso 5b : Sino obtengo la mayor de todas las tags del repo (que no sea RC, SNAPSHOT, ni tenga el 2º dígito mayor a minor) */
                                version = getBiggestVersion(dependencyName, dirClone, credentials, minorVersion, tasklistener);
                            }
                            int secondDigitVersionDependency = Utils.getSecondDigit(dependencyVersion);
                            // Compruebo si la versión obtenida es mayor a la versión de la dependencia o si tiene el 2º dígito mayor a minor, si es así la cambio
                            boolean isGreaterThan = Utils.isGreaterThan(version, dependencyVersion);
                            boolean isMayorSecondDigit = secondDigitVersionDependency > Integer.parseInt(minorVersion);

                            if (isGreaterThan || (isMayorSecondDigit && !"0.0.0".equalsIgnoreCase(version))) {
                                String newLine = line.replace(dependencyVersion, version).replace("^", "");
                                newLines.add(newLine);

                                updatedDependencies.append(dependencyName).append(":").append(version).append(", ");
                                updatedSchamanDependencies.setUpdated(true);
                            } else {
                                // Comprobar que la versión de la dependencia schaman no sea RC o SNAPSHOT y que el 2º dígito sea menor o igual (pete)
                                if (dependencyVersion.contains("SNAPSHOT")) {
                                    throw new RuntimeException("La dependencia " + dependencyName + " del proyecto " + parentProject + " contiene SNAPSHOT : " + dependencyVersion);
                                }

                                if (dependencyVersion.contains("RC")) {
                                    throw new RuntimeException("La dependencia " + dependencyName + " del proyecto " + parentProject + " contiene RC : " + dependencyVersion);
                                }

                                if (isMayorSecondDigit && "0.0.0".equalsIgnoreCase(version)) {
                                    throw new RuntimeException("La versión " + dependencyVersion + " de la dependencia " + dependencyName + " del proyecto " + parentProject + " tiene el 2º dígito mayor a " + minorVersion);
                                }

                                if (line.contains("^")) {
                                    String newLine = line.replace("^", "");
                                    newLines.add(newLine);
                                    updatedDependencies.append(dependencyName).append(":").append(dependencyVersion).append(", ");
                                    updatedSchamanDependencies.setUpdated(true);
                                } else {
                                    newLines.add(line);
                                }
                            }
                        }
                    }
                } else {
                    newLines.add(line);
                }
            }

            String collect = String.join("\n", newLines);
            PrintWriter prw = new PrintWriter(file);
            prw.println(collect);
            prw.close();

            if (updatedDependencies.length() > 0) {
                updatedDependencies = new StringBuilder(updatedDependencies.toString().replaceAll(", $", ". "));
                updatedSchamanDependencies.setDependencies(updatedDependencies.toString());
            }
            return updatedSchamanDependencies;

        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error en readAndUpdateVersionDependencyPackage. FileNotFoundException: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error en readAndUpdateVersionDependencyPackage. IOException: " + e.getMessage());
        }
    }

    protected static String getBiggestVersion(String projectName, String dirClone, org.eclipse.jgit.transport.CredentialsProvider credentials, String minorVersion, TaskListener tasklistener) {
        Git git = null;
        String fullPath = null;
        try {
            git = Utils.cloneRepository(projectName, dirClone, credentials);
            fullPath = dirClone + projectName;
            List<String> allTags = Utils.getTags(git);
            List<String> releaseTags = getReleaseTags(allTags);
            List<String> tags = Utils.getTagsWithEqualOrLesserMinor(releaseTags, minorVersion);

            String maxVersion = "0.0.0";
            for (String version : tags) {
                if (Utils.isGreaterThan(version, maxVersion)) {
                    maxVersion = version;
                }
            }
            return maxVersion;
        } finally {
            if (git != null) {
                git.getRepository().close();
                git.close();
            }

            if (fullPath != null) {
                Utils.deleteClonedDirectory(fullPath, tasklistener);
            }
        }
    }

    public static List<String> getReleaseTags(List<String> tags) {
        return tags.stream().filter(t -> {
            if (t.contains("SNAPSHOT") || t.contains("RC")) {
                return false;
            }

            return !t.contains("SNAPSHOT") && !t.contains("RC");

        }).collect(Collectors.toList());
    }

    protected static void mergeProcessNotAdmitConflicts(
            Git git,
            TaskListener taskListener,
            String checkoutBranch,
            String branch,
            org.eclipse.jgit.transport.CredentialsProvider credentialsProvider
    ) throws GitAPIException, IOException {
        // Comprobar que existen las ramas
        boolean existsBranch = Utils.existsBranch(git, checkoutBranch, credentialsProvider);
        if (!existsBranch) {
            Utils.log(taskListener, "No existe la rama " + checkoutBranch);
        }

        existsBranch = Utils.existsBranch(git, branch, credentialsProvider);
        if (!existsBranch) {
            Utils.log(taskListener, "No existe la rama " + branch);
        }

        // Si git no está en la rama checkoutBranch, se hace el checkout a la rama checkoutBranch (para que no de error de que ya está en esa rama al hacer checkout)
        String currentBranch = git.getRepository().getFullBranch();
        if (!currentBranch.contains(checkoutBranch)) {
            Utils.checkout(git, checkoutBranch);
            currentBranch = git.getRepository().getFullBranch();
            if (!currentBranch.contains(checkoutBranch)) {
                throw new RuntimeException("Git no se ha podido cambiar a la rama " + checkoutBranch);
            }
            Utils.log(taskListener, "Se ha hecho ckeckout a la rama " + checkoutBranch);
        }

        ObjectId branchObjectId = git.getRepository().resolve("refs/heads/" + branch);

        String commitMessage = "Merge " + branch + " a " + checkoutBranch + ". ";

        MergeResult mergeResult = git.merge()
                .setCommit(false)
                .include(branchObjectId).call();
        Utils.log(taskListener, "Merge " + branch + " a " + checkoutBranch);

        Map<String, int[][]> conflicts = mergeResult.getConflicts();

        if (conflicts != null) {
            String infoConflicts = Utils.getInfoConflicts(conflicts);
            Utils.log(taskListener, "Conflictos en el merge de " + branch + " a " + checkoutBranch + ":\n" + infoConflicts);
            throw new RuntimeException(infoConflicts);
        }

        Utils.add(git);
        Utils.commit(git, commitMessage);
    }

    protected static List<String> getRcTags(List<String> tags) {
        return tags.stream().filter(t -> t.contains("RC")).collect(Collectors.toList());
    }

    protected static boolean deleteBranch(Git git, String branch, org.eclipse.jgit.transport.CredentialsProvider credentialsProvider) {
        try {
            //delete branch "branchToDelete" locally
            git.branchDelete().setBranchNames("refs/heads/" + branch).call();

            //delete branch "branchToDelete" on remote "origin"
            RefSpec refSpec = new RefSpec()
                    .setSource(null)
                    .setDestination("refs/heads/" + branch);
            git.push().setRefSpecs(refSpec).setRemote("origin").setCredentialsProvider(credentialsProvider).call();
            return !Utils.existsBranch(git, branch, credentialsProvider);

        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido eliminar la rama " + branch + ". " + e.getMessage());
        }
    }
}
