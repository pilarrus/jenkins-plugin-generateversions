package com.schaman.jenkins.plugin.generateversions;

import com.schaman.jenkins.plugin.globalconfigurations.Project;
import com.ximpleware.*;
import hudson.FilePath;
import hudson.model.Job;
import hudson.model.Run;
import hudson.model.TaskListener;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Snapshot {
    protected static void generateVersions(
            Run<?, ?> run,
            FilePath workspace,
            TaskListener taskListener,
            org.eclipse.jgit.transport.CredentialsProvider credentialsProvider,
            String minorValue,
            long maxWait,
            long sleepWait,
            List<GenerateVersionsProject> projects,
            List<GenerateVersionsProject> compiledProjects,
            Set<Project> globalProjects,
            Zoom zoom
    ) throws IOException, GitAPIException, InterruptedException {
        for (GenerateVersionsProject project : projects) {
            Git git = null;
            String fullPath = null;
            String nameDirClone = UUID.randomUUID().toString();
            String dirClone = workspace.getRemote() + File.separator + nameDirClone + File.separator;

            try {
                String projectName = project.getProjectName();
                if (StringUtils.isBlank(projectName)) {
                    throw new RuntimeException("El nombre del proyecto no puede estar vacío");
                }
                Utils.log(taskListener, "Nombre del proyecto: " + projectName);

                String zoomMsg = "Comienzan las operaciones necesarias para generar la versión SNAPSHOT del proyecto " + projectName;
                Utils.log(taskListener, zoomMsg);
                zoom.setBody(zoomMsg);
                // TODO : Descomentar notificar por Zoom
//                zoom.notifyZoom();

                fullPath = dirClone + projectName;
                git = Utils.cloneRepository(projectName, dirClone, credentialsProvider);
                Repository repository = git.getRepository();

                /* Paso 1 : Borrar todas las tags que tienen minor igual al actual minor y sean SNAPSHOT */
                List<String> tags = Utils.getTags(git);
                List<String> tagsToDelete = Utils.getTagsWithSameMinorAndSnapshot(tags, minorValue);
                if (tagsToDelete.size() > 0) {
                    Utils.log(taskListener, "Nº de tags antes de borrar las tags que sean *." + minorValue + ".*-SNAPSHOT: " + tags.size());
                    Utils.log(taskListener, "Tags para borrar: " + tagsToDelete.toString());
                    Utils.deleteTags(git, tagsToDelete, credentialsProvider);
                    tags = Utils.getTags(git);
                    Utils.log(taskListener, "Nº de tags después de borrar: " + tags.size());
                } else {
                    Utils.log(taskListener, "No hay tags *." + minorValue + ".*-SNAPSHOT para borrar");
                }

                /* Paso 2 : Si la rama existe situarse en ella, sino pasar al siguiente proyecto */
                String branch = "v" + minorValue;
                boolean existsBranch = Utils.existsBranch(git, branch, credentialsProvider);
                if (!existsBranch) {
                    Utils.log(taskListener, "No existe la rama " + branch);
                    continue;
                }

                Utils.checkout(git, branch);
                String currentBranch = repository.getFullBranch();
                if (!currentBranch.contains(branch)) {
                    throw new RuntimeException("Git no se ha podido cambiar a la rama " + branch + " sigue en master del proyecto " + projectName);
                }
                Utils.log(taskListener, "Git se ha cambiado a la rama: " + branch);

                /* Paso 3 : Obtener la versión actual del pom */
                File pomFile = Utils.getFile(repository, "pom.xml");
                String versionPom = null;
                if (pomFile != null) {
                    versionPom = Utils.getVersionPom(pomFile);
                    if (versionPom == null) {
                        throw new RuntimeException("No se ha podido obtener la versión del pom.xml del proyecto " + projectName);
                    }

                    if (!versionPom.contains("-SNAPSHOT")) {
                        throw new RuntimeException("La versión " + versionPom + " del pom.xml del proyecto " + projectName + " no contiene -SNAPSHOT");
                    }
                    Utils.log(taskListener, "Versión obtenida del pom.xml: " + versionPom);
                }

                /* Paso 3 : Obtener la versión actual del package.json (si tiene el fichero) */
                File packageFile = Utils.getFile(repository, "package.json");
                String versionPackage = null;
                if (packageFile != null) {
                    versionPackage = Utils.getVersionPackage(packageFile);
                    if (versionPackage == null) {
                        throw new RuntimeException("No se ha podido obtener la versión del package.json del proyecto " + projectName);
                    }

                    if (!versionPackage.contains("-SNAPSHOT")) {
                        throw new RuntimeException("La versión " + versionPackage + " del package.json del proyecto " + projectName + " no contiene -SNAPSHOT");
                    }
                    Utils.log(taskListener, "Versión obtenida del package.json: " + versionPackage);
                }

                if (pomFile == null && packageFile == null) {
                    throw new RuntimeException("El proyecto " + projectName + " no tiene pom.xml ni package.json");
                }

                /*MERGE*/
                Utils.mergeProcess(git, taskListener, branch, credentialsProvider, pomFile, versionPom, packageFile, versionPackage);
                /*FIN MERGE*/

                String commitMessage = "";
                // Si tiene package.json cambiar el 4º dígito de la versión por BUILD_TIMESTAMP
                boolean updatedVersionPackage = false;
                if (packageFile != null) {
                    // Si la versión de package tiene un 4º dígito se elimina
                    String[] split = versionPackage.split("-SNAPSHOT");
                    if (split.length > 1) {
                        String versionWithoutTT = split[0] + "-SNAPSHOT";
                        versionPackage = versionWithoutTT;
                        Utils.log(taskListener, "Eliminado el 4 dígito de la versión del package.json: " + versionPackage);
                    }
                    // Se obtiene el 4º dígito que se le va a añadir a la versión
                    String buildTimestamp = run.getEnvironment(taskListener).get("BUILD_TIMESTAMP");
                    Utils.log(taskListener, "buildTimestamp: " + buildTimestamp);

                    // Se crea la nueva versión del package.json añadiendo buildTimestamp como 4º dígito
                    String newVersionPackage = versionPackage + "." + buildTimestamp;
                    Utils.log(taskListener, "newVersionPackage: " + newVersionPackage);
                    updatedVersionPackage = Utils.updateVersionPackage(packageFile, newVersionPackage);

                    if (updatedVersionPackage) {
                        // Se obtiene la versión del package.json para comprobar que la ha cambiado
                        String currentVersionPackage = Utils.getVersionPackage(packageFile);
                        Utils.log(taskListener, "currentVersionPackage: " + currentVersionPackage);

                        if (!currentVersionPackage.equals(newVersionPackage)) {
                            throw new RuntimeException("No se ha podido modificar la versión del package.json");
                        }

                        String msg = "Se ha cambiado la versión del package.json por " + currentVersionPackage + ". ";
                        Utils.log(taskListener, msg);
                        commitMessage += msg;
                    }
                }

                /* Paso 4 : Leer dependencias schaman de los ficheros pom.xml y actualizar su versión si es necesario */
                boolean updatedDependenciesPom = false;
                // TODO : Comprobar que updatedSchamanDependencies no se sobreescribe en cada iteración
//                UpdatedSchamanDependencies updatedSchamanDependencies = null;
                UpdatedSchamanDependencies updatedSchamanDependencies = new UpdatedSchamanDependencies();
                List<File> pomFiles = Utils.getPoms(repository, taskListener);
                for (File pomXml : pomFiles) {
//                    updatedSchamanDependencies = readAndUpdateSchamanDependencies(pomXml, compiledProjects, globalProjects, dirClone, credentialsProvider, minorValue, projectName, taskListener);
//                    updatedSchamanDependencies = readAndUpdateSchamanDependencies(pomXml, updatedSchamanDependencies, compiledProjects, globalProjects, dirClone, credentialsProvider, minorValue, projectName, taskListener);
                    readAndUpdateVersionDependenciesPom(pomXml, updatedSchamanDependencies, compiledProjects, globalProjects, dirClone, credentialsProvider, minorValue, projectName, taskListener);
                    boolean anyUpdatedDependency = updatedSchamanDependencies.isUpdated();
                    if (anyUpdatedDependency) {
                        updatedDependenciesPom = true;
                    }
                }

                if (pomFile != null) {
                    Utils.log(taskListener, "Se han actualizado las dependencias del pom.xml: " + updatedDependenciesPom);
                }

                if (updatedDependenciesPom) {
                    String msg = "Se ha actualizado la versión de las dependencias del pom.xml " + updatedSchamanDependencies.getDependencies();
                    Utils.log(taskListener, msg);
                    commitMessage += msg;
                }

                boolean updatedDependenciesPackage = false;
                UpdatedSchamanDependencies updatedSchamanDependenciesPackage = null;
                if (packageFile != null) {
                    updatedSchamanDependenciesPackage = readAndUpdateVersionDependencyPackage(packageFile, compiledProjects, dirClone, credentialsProvider, minorValue, projectName, taskListener);
                    boolean anyUpdatedDependency = updatedSchamanDependenciesPackage.isUpdated();
                    if (anyUpdatedDependency) {
                        updatedDependenciesPackage = true;
                    }
                }

                if (packageFile != null) {
                    Utils.log(taskListener, "Se han actualizado las dependencias del package.json: " + updatedDependenciesPackage);
                }

                if (updatedDependenciesPackage) {
                    String msg = "Se ha actualizado la versión de las dependencias del package.json " + updatedSchamanDependenciesPackage.getDependencies();
                    Utils.log(taskListener, msg);
                    commitMessage += msg;
                }


                /* Paso 5 : Si se ha actualizado la versión del package.json o alguna dependencia -> commit + push */
                if (updatedVersionPackage || updatedDependenciesPom || updatedDependenciesPackage) {
                    Utils.add(git);
                    Utils.commit(git, commitMessage);
                    Utils.push(git, branch, credentialsProvider);
                    Utils.log(taskListener, "Se ha hecho commit + push de los cambios en la rama " + branch);
                }

                /* Paso 6 : push tag (lanza compilación de la tarea) */
                long startDate = System.currentTimeMillis();
                String version = versionPom != null ? versionPom : versionPackage;
                Utils.log(taskListener, "version: " + version);

                boolean isCreatedTag = Utils.createTag(git, version, credentialsProvider);
                if (!isCreatedTag) {
                    throw new RuntimeException("No se ha podido crear la tag " + version + " en el repositorio remoto del proyecto " + projectName);
                }
                Utils.log(taskListener, "Se ha creado la tag: " + version);

                /* Paso 7 : Se espera hasta conocer el estado de compilación de la tarea */
                Project projectSchaman = Utils.getProjectSchamanByProjectName(globalProjects, projectName);
                if (projectSchaman == null) {
                    throw new RuntimeException("No se ha podido encontrar el proyecto " + projectName + " en la lista de proyectos de Schaman");
                }
                String jobName = projectSchaman.getJobName();

                Job<?, ?> job = Utils.getJob(jobName, taskListener);
                if (job == null) {
                    throw new RuntimeException("No existe una tarea en Jenkins con el nombre " + jobName);
                }

                boolean compiled = Utils.isJobCompiled(job, maxWait, startDate, taskListener, sleepWait);
                /* 7.b : Error. Fin de la ejecución */
                if (!compiled) {
                    throw new RuntimeException("Tiempo máximo excedido, no se ha compilado la tarea " + jobName);
                }

                /* Paso 8 : Se añade proyecto a la lista de compilados */
                project.setVersion(version);
                project.setStatus("OK");
                compiledProjects.add(project);
                Utils.log(taskListener, "Se ha añadido " + projectName + " a proyectos compilados");

            } catch (Throwable e) {
                project.setStatus("KO");
                throw e;
            } finally {
                if (git != null) {
                    git.getRepository().close();
                    git.close();
                }

                if (fullPath != null) {
                    Utils.deleteClonedDirectory(fullPath, taskListener);
                    Utils.deleteClonedDirectory(dirClone, taskListener);
                }

                Utils.log(taskListener, "-----------------------------------------------------");
            }
        }
    }

    protected static void readAndUpdateVersionDependenciesPom(
            File pomFile,
            UpdatedSchamanDependencies updatedSchamanDependencies,
            List<GenerateVersionsProject> compiledProjects,
            Set<Project> globalProjects,
            String dirClone,
            org.eclipse.jgit.transport.CredentialsProvider credentials,
            String minorVersion,
            String parentProjectName,
            TaskListener tasklistener
    ) {
//        UpdatedSchamanDependencies updatedSchamanDependencies = new UpdatedSchamanDependencies();
        StringBuilder updatedDependencies = new StringBuilder();
        String pomPath = pomFile.getAbsolutePath();
        try {
            int i;
            VTDGen vg = new VTDGen();
            if (!vg.parseFile(pomPath, true)) {
                throw new RuntimeException("No se ha podido parsear el pom.xml");
            }
            VTDNav vn = vg.getNav();
            XMLModifier xm = new XMLModifier(vn);
            AutoPilot ap = new AutoPilot(vn);

            ap.selectXPath("/project/dependencies/dependency");
            while ((i = ap.evalXPath()) != -1) {
                if (vn.toElement(VTDNav.FIRST_CHILD, "groupId")) {
                    String groupId = Utils.toNormalizedStringText(vn);

                    if ("schaman".equalsIgnoreCase(groupId) || "schaman-hdm".equalsIgnoreCase(groupId)) {
                        if (vn.toElement(VTDNav.NEXT_SIBLING, "artifactId")) {
                            String artifactId = Utils.toNormalizedStringText(vn);

                            if (vn.toElement(VTDNav.NEXT_SIBLING, "version")) {
                                String versionDependency = Utils.toNormalizedStringText(vn);

                                Project projectSchaman = Utils.getProjectSchamanByArtifactId(globalProjects, artifactId);
                                if (projectSchaman == null) {
                                    throw new RuntimeException("No se ha podido encontrar el proyecto con el artifactID " + artifactId);
                                }

                                String projectName = projectSchaman.getProjectName();
//                                String version = "0.0.0";
                                String version;
                                /* Paso 5 : Comprobar si ha sido compilado */
                                if (Utils.isCompiled(compiledProjects, projectName)) {
                                    /* Paso 5a : Si ha sido compilado cojo la versión del proyecto compilado */
                                    version = Utils.getCompiledVersion(compiledProjects, projectName);
                                    if (version == null) {
                                        throw new RuntimeException("No se ha podido encontrar la version en el proyecto compilado");
                                    }
                                } else {
                                    /* Paso 5b : Sino obtengo la mayor de todas las tags del repo (que no tenga el 2º dígito mayor a minor) */
                                    version = getBiggestVersion(projectName, dirClone, credentials, minorVersion, tasklistener);
                                }
                                int secondDigitVersionDependency = Utils.getSecondDigit(versionDependency);
                                // Si version es mayor que versionDependency o el segundo dígito es mayor que minor actualizar en el pom
                                if (Utils.isGreaterThan(version, versionDependency) || (secondDigitVersionDependency > Integer.parseInt(minorVersion) && !"0.0.0".equalsIgnoreCase(version))) {
                                    // cambiar versión
                                    int j = vn.getText();
                                    xm.updateToken(j, version);
                                    // guardar cambios en el pom
                                    xm.output(pomPath);
                                    updatedSchamanDependencies.setUpdated(true);
                                    updatedDependencies.append(artifactId).append(":").append(version).append(", ");
                                } else {
                                    // Comprobar que la versión de la dependencia tiene el 2º dígito menor o igual a minor (pete)
                                    if (secondDigitVersionDependency > Integer.parseInt(minorVersion) && "0.0.0".equalsIgnoreCase(version)) {
                                        throw new RuntimeException("La versión " + versionDependency + " de la dependencia " + artifactId + " del proyecto " + parentProjectName + " tiene el 2º dígito mayor a " + minorVersion);
                                    }
                                }
                            }
                        }
                    }
                }
                vn.toElement(VTDNav.PARENT);
            }

            if (updatedDependencies.length() > 0) {
                updatedDependencies = new StringBuilder(updatedDependencies.toString().replaceAll(", $", ". "));
                updatedSchamanDependencies.setDependencies(updatedDependencies.toString());
            }
//            return updatedSchamanDependencies;

        } catch (NavException e) {
            throw new RuntimeException("Error NavException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (XPathParseException e) {
            throw new RuntimeException("Error XPathParseException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (XPathEvalException e) {
            throw new RuntimeException("Error XPathEvalException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (ModifyException e) {
            throw new RuntimeException("Error ModifyException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error IOException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        } catch (TranscodeException e) {
            throw new RuntimeException("Error TranscodeException en el método readAndUpdateSchamanDependencies" + e.getMessage());
        }
    }

    protected static UpdatedSchamanDependencies readAndUpdateVersionDependencyPackage(
            File packageFile,
            List<GenerateVersionsProject> compiledProjects,
            String dirClone,
            org.eclipse.jgit.transport.CredentialsProvider credentials,
            String minorVersion,
            String parentProjectName,
            TaskListener tasklistener
    ) {
        UpdatedSchamanDependencies updatedSchamanDependencies = new UpdatedSchamanDependencies();
        StringBuilder updatedDependencies = new StringBuilder();
        try {
            List<String> newLines = new ArrayList<>();
            List<String> lines = Files.readAllLines(Paths.get(packageFile.getAbsolutePath()));

            for (String line : lines) {
                String lineWithoutSpaces = line.replaceAll("\\s", "");
                if (lineWithoutSpaces.trim().startsWith("\"@schaman/") && line.contains("@schaman/")) {
                    lineWithoutSpaces = lineWithoutSpaces.replaceAll("\"", "").replaceAll(",", "");
                    String[] split1 = lineWithoutSpaces.split("/");
                    if (split1.length > 0) {
//                        String groupId = split1[0];
                        String dependencyAndVersion = split1[1];
                        String[] split2 = dependencyAndVersion.split(":");
                        if (split2.length > 0) {
                            String dependencyName = split2[0];
                            String dependencyVersion = split2[1].replace("^", "");
                            Utils.log(tasklistener, "Versión de la dependencia " + dependencyName + ": " + dependencyVersion);

                            String[] split = dependencyVersion.split("-SNAPSHOT");
                            boolean containsTimestamp = split.length > 1;

                            String dependencyVersionWithoutTimestamp = containsTimestamp ? split[0] + "-SNAPSHOT" : dependencyVersion;
                            Utils.log(tasklistener, "Versión de la dependencia " + dependencyName + " sin timestamp: " + dependencyVersionWithoutTimestamp);

//                            String version = "0.0.0";
                            String version;
                            // Compruebo si es un proyecto compilado
                            boolean isCompiled = Utils.isCompiled(compiledProjects, dependencyName);
                            if (isCompiled) {
                                /* Paso 5a : Si ha sido compilado cojo la versión del proyecto compilado */
                                version = Utils.getCompiledVersion(compiledProjects, dependencyName);
                                if (version == null) {
                                    throw new RuntimeException("No se ha podido encontrar la version en el proyecto compilado");
                                }
                            } else {
                                /* Paso 5b : Sino obtengo la mayor de todas las tags del repo (que no sea RC, SNAPSHOT, ni tenga el 2º dígito mayor a minor) */
                                version = getBiggestVersion(dependencyName, dirClone, credentials, minorVersion, tasklistener);
                            }
                            int secondDigitVersionDependency = Utils.getSecondDigit(dependencyVersionWithoutTimestamp);
                            // Compruebo si la versión obtenida es mayor a la versión de la dependencia o si tiene el 2º dígito mayor a minor, si es así la cambio
                            boolean isGreaterThan = Utils.isGreaterThan(version, dependencyVersionWithoutTimestamp);
                            boolean isMayorSecondDigit = secondDigitVersionDependency > Integer.parseInt(minorVersion);

                            if (isGreaterThan || (isMayorSecondDigit && !"0.0.0".equalsIgnoreCase(version))) {
                                String newVersionDependency = !line.contains("^") ? "^" + version : version;
                                Utils.log(tasklistener, "Nueva versión de la dependencia " + dependencyName + ": " + newVersionDependency);
                                String newLine = line.replace(dependencyVersion, newVersionDependency);
                                newLines.add(newLine);

                                updatedDependencies.append(dependencyName).append(":").append(newVersionDependency).append(", ");
                                updatedSchamanDependencies.setUpdated(true);
                            } else {
                                // Comprobar que la versión de la dependencia schaman tenga el 2º dígito menor o igual a minor (pete)
                                if (isMayorSecondDigit && "0.0.0".equalsIgnoreCase(version)) {
                                    throw new RuntimeException("La versión " + dependencyVersion + " de la dependencia " + dependencyName + " del proyecto " + parentProjectName + " tiene el 2º dígito mayor a " + minorVersion);
                                }

                                // Si la versión de la dependencia no contiene ^ añadirlo y si tiene timestamp quitarlo
                                if (!line.contains("^") || containsTimestamp) {
                                    String newVersionDependency = dependencyVersion;
                                    if (!line.contains("^")) {
                                        newVersionDependency = "^" + dependencyVersion;
                                        Utils.log(tasklistener, "Se ha añadido ^ a la versión de la dependencia " + dependencyName + ": " + newVersionDependency);
                                    }
                                    if (containsTimestamp) {
                                        newVersionDependency = newVersionDependency.replace(dependencyVersion, dependencyVersionWithoutTimestamp);
                                        Utils.log(tasklistener, "Se le ha quitado el timestamp a la versión de la dependencia " + dependencyName + ": " + newVersionDependency);
                                    }

                                    String newLine = line.replace(dependencyVersion, newVersionDependency);
                                    newLines.add(newLine);

                                    updatedDependencies.append(dependencyName).append(":").append(newVersionDependency).append(", ");
                                    updatedSchamanDependencies.setUpdated(true);
                                } else {
                                    newLines.add(line);
                                }
                            }
                        }
                    }
                } else {
                    newLines.add(line);
                }
            }

            String collect = String.join("\n", newLines);
            PrintWriter prw = new PrintWriter(packageFile);
            prw.println(collect);
            prw.close();

            if (updatedDependencies.length() > 0) {
                updatedDependencies = new StringBuilder(updatedDependencies.toString().replaceAll(", $", ". "));
                updatedSchamanDependencies.setDependencies(updatedDependencies.toString());
            }
            return updatedSchamanDependencies;

        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error en readAndUpdateVersionDependencyPackage. FileNotFoundException: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error en readAndUpdateVersionDependencyPackage. IOException: " + e.getMessage());
        }
    }

    protected static String getBiggestVersion(String projectName, String dirClone, org.eclipse.jgit.transport.CredentialsProvider credentials, String minorVersion, TaskListener tasklistener) {
        Git git = null;
        String fullPath = null;
        try {
            git = Utils.cloneRepository(projectName, dirClone, credentials);
            fullPath = dirClone + projectName;
            List<String> allTags = Utils.getTags(git);
            List<String> tags = Utils.getTagsWithEqualOrLesserMinor(allTags, minorVersion);

            String maxVersion = "0.0.0";
            for (String version : tags) {
                if (Utils.isGreaterThan(version, maxVersion)) {
                    maxVersion = version;
                }
            }
            return maxVersion;
        } finally {
            if (git != null) {
                git.getRepository().close();
                git.close();
            }

            if (fullPath != null) {
                Utils.deleteClonedDirectory(fullPath, tasklistener);
            }
        }
    }

}
