package com.schaman.jenkins.plugin.generateversions;

import com.schaman.jenkins.plugin.globalconfigurations.Project;
import com.schaman.jenkins.plugin.globalconfigurations.Projects;
import hudson.Extension;
import hudson.Util;
import hudson.model.AbstractDescribableImpl;
import hudson.model.Descriptor;
import hudson.model.Item;
import hudson.util.FormValidation;
import hudson.util.ListBoxModel;
import jenkins.model.Jenkins;
import org.apache.commons.lang.StringUtils;
import org.jenkinsci.Symbol;
import org.kohsuke.stapler.AncestorInPath;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.verb.POST;

import javax.annotation.Nonnull;
import java.util.Set;

public class GenerateVersionsProject extends AbstractDescribableImpl<GenerateVersionsProject> {
    private final String projectName;
    private String version;
    private String status;

    @DataBoundConstructor
    public GenerateVersionsProject(String projectName) {
        this.projectName = StringUtils.trim(projectName);
        this.version = null;
        this.status = "No compilado";
    }

    public String getProjectName() {
        return projectName;
    }

//    @DataBoundSetter
//    public void setProjectName(String projectName) {
//        this.projectName = projectName;
//    }

    public String getVersion() {
        return version;
    }

    @DataBoundSetter
    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    @DataBoundSetter
    public void setStatus(String status) {
        this.status = status;
    }

    @Symbol("generateVersionsProject")
    @Extension
    public static class DescriptorImpl extends Descriptor<GenerateVersionsProject> {

        public Set<Project> getSchamanProjects() {
            Projects.DescriptorImpl descriptorByType = Jenkins.get().getDescriptorByType(Projects.DescriptorImpl.class);
            return descriptorByType.getProjects();
        }

        public ListBoxModel doFillProjectNameItems(@AncestorInPath final Item item, @QueryParameter final String projectName) throws NoSuchFieldException {
            Set<Project> projectsSchaman = getSchamanProjects();

            ListBoxModel items = new ListBoxModel();
            for (Project projectSchaman : projectsSchaman) {
                if (!projectSchaman.getProjectName().trim().isEmpty()) {
                    items.add(projectSchaman.getProjectName());
                }
            }

            boolean isDeleted = true;
            for (Project project : projectsSchaman) {
                if (projectName.equals(project.getProjectName())) {
                    isDeleted = false;
                }
            }
            if (isDeleted) {
                items.add(projectName);
            }

            return items;
        }

        @POST
        public FormValidation doCheckProjectName(@QueryParameter String projectName, @AncestorInPath Item item) {
            if (Util.fixEmptyAndTrim(projectName) == null) {
                return FormValidation.error("Project cannot be empty");
            }

            // Si se referencia a un proyecto eliminado de la config global, mostrar error
            Set<Project> projectsSchaman = getSchamanProjects();
            boolean isDeleted = true;
            for (Project project : projectsSchaman) {
                if (projectName.equals(project.getProjectName())) {
                    isDeleted = false;
                }
            }
            if (isDeleted) {
                return FormValidation.error(projectName + " está haciendo referencia a un proyecto eliminado de la configuración global.");
            }

            return FormValidation.ok();
        }

        @Nonnull
        @Override
        public String getDisplayName() {
            return "";
        }
    }
}
