package com.schaman.jenkins.plugin.generateversions;

import com.g00fy2.versioncompare.Version;
import com.schaman.jenkins.plugin.globalconfigurations.Project;
import com.schaman.jenkins.plugin.globalconfigurations.Projects;
import com.ximpleware.*;
import hudson.model.Job;
import hudson.model.Run;
import hudson.model.TaskListener;
import jenkins.model.Jenkins;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.RefSpec;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {
    public static final String VERSION_REGEX_POM = "<\\s*version[^>]*>(.*?)<\\s*/\\s*version>";
    public static final String VERSION_REGEX_PACKAGE = "\\s*\\\"\\s*version\\s*\\\"\\s*\\:\\s*\\\"(.*)";

    protected static void log(TaskListener listener, String message) {
        listener.getLogger().println("[Generate Version] " + message + "\n");
    }

    protected static Set<Project> getProjectsSchaman() {
        Projects.DescriptorImpl descriptorByType = Jenkins.get().getDescriptorByType(Projects.DescriptorImpl.class);
        return descriptorByType.getProjects();
    }

    protected static Project getProjectSchamanByProjectName(Set<Project> projects, String projectName) {
        for (Project project : projects) {
            if (project.getProjectName().equals(projectName)) {
                return project;
            }
        }
        return null;
    }

    protected static Project getProjectSchamanByArtifactId(Set<Project> projects, String artifactId) {
        for (Project project : projects) {
            if (project.getArtifactId().equals(artifactId)) {
                return project;
            }
        }
        return null;
    }

    //  TODO : Cambiar URI por la de Schaman
    protected static Git cloneRepository(String projectName, String dirClone, org.eclipse.jgit.transport.CredentialsProvider credentials) {
        try {
            return Git.cloneRepository()
                    .setURI("https://pilarrus@bitbucket.org/pilarrus/" + projectName + ".git")
//                    .setURI("https://desarrollo_ci_bluu@bitbucket.org/bluu-software/" + projectName + ".git")
                    .setCredentialsProvider(credentials)
                    .setDirectory(new File(dirClone + projectName))
                    .call();
        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido clonar el proyecto " + projectName + ". " + e.getMessage());
        }
    }

    protected static List<String> getTags(Git git) {
        try {
            List<Ref> tagsRefs = git.tagList().call();
            List<String> tags = new ArrayList<>();
            for (Ref tag : tagsRefs) {
                tags.add(tag.getName().replaceAll("refs/tags/", ""));
            }
            return tags;
        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido obtener las tags. " + e.getMessage());
        }
    }

    protected static List<String> getTagsWithSameMinorAndSnapshot(List<String> tags, String minor) {
        return tags.stream().filter(t -> {
            String[] digits = t.split("\\.");
            if (digits.length != 3) {
                return false;
            }
            if (!digits[2].contains("SNAPSHOT")) {
                return false;
            }

            return digits[1].equals(minor);

        }).collect(Collectors.toList());
    }

    protected static void deleteTags(Git git, List<String> tagsToDelete, org.eclipse.jgit.transport.CredentialsProvider credentials) {
        for (String tagToDelete : tagsToDelete) {
            boolean result = deleteTag(git, tagToDelete, credentials);
            if (!result) {
                throw new RuntimeException("No se ha borrado la tag " + tagToDelete);
            }
        }
    }

    protected static boolean deleteTag(Git git, String tagToDelete, org.eclipse.jgit.transport.CredentialsProvider credentials) {
        try {
            // Borrar en local
            git.branchDelete().setBranchNames("refs/tags/" + tagToDelete).setForce(true).call();

            // Borrar en remoto
            RefSpec refSpec = new RefSpec()
                    .setSource(null)
                    .setDestination("refs/tags/" + tagToDelete);
            git.push().setCredentialsProvider(credentials).setRefSpecs(refSpec).setRemote("origin").call();

            return isDeleted(git, tagToDelete);

        } catch (GitAPIException e) {
            throw new RuntimeException("Ha habido un error al intentar borrar la tag " + tagToDelete + ". " + e.getMessage());
        }
    }

    protected static boolean isDeleted(Git git, String deletedTag) {
        List<String> tags = getTags(git);
        for (String tag : tags) {
            if (tag.equalsIgnoreCase(deletedTag)) {
                return false;
            }
        }
        return true;
    }

    protected static boolean existsBranch(Git git, String label, org.eclipse.jgit.transport.CredentialsProvider credentials) {
        Collection<Ref> branches = lsRemote(git, credentials);
        for (Ref ref : branches) {
            if (ref.getName().endsWith("/" + label)) {
                return true;
            }
        }
        return false;
    }

    protected static Collection<Ref> lsRemote(Git git, org.eclipse.jgit.transport.CredentialsProvider credentials) {
        try {
            return git.lsRemote()
                    .setCredentialsProvider(credentials)
                    .setRemote("origin")
                    .setTags(true)
                    .setHeads(true)
                    .call();
        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido listar las ramas/tags. " + e.getMessage());
        }
    }

    protected static void checkout(Git git, String branch) {
        try {
            if ("master".equalsIgnoreCase(branch)) {
                git.checkout().setCreateBranch(false).setName(branch).setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK).setStartPoint("origin/" + branch).call();
            } else {
                git.checkout().setCreateBranch(true).setName(branch).setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK).setStartPoint("origin/" + branch).call();
            }
        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido cambiar a la rama " + branch + ". " + e.getMessage());
        }
    }

    protected static File getFile(Repository repository, String nameSearchedFile) {
        File directoryGit = repository.getDirectory().getAbsoluteFile();
        String pathDirectory = directoryGit.getPath().replaceAll("\\.git", "");
        File directory = new File(pathDirectory);
        return searchFile(directory, nameSearchedFile);
    }

    protected static File searchFile(File parentDirectory, String nameSearchedFile) {
        File[] files = parentDirectory.listFiles();
        if (files == null) {
            return null;
        }

        File searchedFile = null;

        for (File file : files) {
            if (file.isDirectory()) {
                searchedFile = searchFile(file.getAbsoluteFile(), nameSearchedFile);
                if (searchedFile != null) {
                    return searchedFile;
                }
            } else {
                String fileName = file.getName();
                if (fileName.contains(nameSearchedFile)) {
                    searchedFile = file.getAbsoluteFile();
                    return searchedFile;
                }
            }
        }

        return searchedFile;
    }

//    protected static File getPom(Repository repository) {
//        File directoryGit = repository.getDirectory().getAbsoluteFile();
//        String pathDirectory = directoryGit.getPath().replaceAll("\\.git", "");
//        File directory = new File(pathDirectory);
//        return searchPom(directory);
//    }

//    protected static File searchPom(File parentDirectory) {
//        File[] files = parentDirectory.listFiles();
//        if (files == null) {
//            return null;
//        }
//        for (File file : files) {
//            String fileName = file.getName();
//            if (fileName.contains("pom.xml")) {
//                return file.getAbsoluteFile();
//            }
//        }
//        return null;
//    }

    protected static List<File> getPoms(Repository repository, TaskListener taskListener) {
        File directoryGit = repository.getDirectory().getAbsoluteFile();
        String pathDirectory = directoryGit.getPath().replaceAll("\\.git", "");
        File directory = new File(pathDirectory);
        List<File> poms = searchPoms(directory);
        log(taskListener, "poms: " + poms);
        return poms;
    }

    protected static List<File> searchPoms(File parentDirectory) {
        List<File> poms = new ArrayList<>();
        File[] files = parentDirectory.listFiles();
        if (files == null) {
            return null;
        }

        for (File file : files) {
            if (file.isDirectory()) {
                poms.addAll(Objects.requireNonNull(searchPoms(file.getAbsoluteFile())));
            } else {
                String fileName = file.getName();
                if (fileName.contains("pom.xml")) {
                    poms.add(file.getAbsoluteFile());
                }
            }
        }
        return poms;
    }

    protected static String getVersionPom(File pom) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            try (FileInputStream fis = new FileInputStream(pom)) {
                Document doc = dBuilder.parse(fis, "UTF-8");
                doc.getDocumentElement().normalize();
                org.w3c.dom.Node project = doc.getFirstChild();
                NodeList childNodes = project.getChildNodes();

                for (int i = 0; i < childNodes.getLength(); i++) {
                    org.w3c.dom.Node node = childNodes.item(i);
                    String nodeName = node.getNodeName();

                    if ("version".equals(nodeName)) {
                        return node.getTextContent();
                    }
                }
            }
            return null;

        } catch (ParserConfigurationException e) {
            throw new RuntimeException("Grave error de configuración en el método getVersion. ParserConfigurationException: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Se ha producido un error en la entrada/salida en el método getVersion. IOException: " + e.getMessage());
        } catch (SAXException e) {
            throw new RuntimeException("Error al parsear el pom en el método getVersion. SAXException: " + e.getMessage());
        }
    }

    protected static String getVersionPackage(File packageJson) {
        String content = getContentFile(packageJson);
        JSONObject jsonObject = new JSONObject(content);
        return jsonObject.getString("version");
    }

    protected static String getContentFile(File file) {
        try {
            StringBuilder newLines = new StringBuilder();
            List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()));
            for (String line : lines) {
                newLines.append(line).append("\n");
            }
            String fileContent = newLines.toString();
            fileContent = fileContent.substring(0, fileContent.length() - 1);
            return fileContent;
        } catch (IOException e) {
            throw new RuntimeException("No se ha podido leer el contenido de " + file.getName() + ". IOException: " + e.getMessage());
        }
    }

    protected static void mergeProcess(
            Git git,
            TaskListener taskListener,
            String branch,
            org.eclipse.jgit.transport.CredentialsProvider credentialsProvider,
            File pomFile,
            String versionPom,
            File packageFile,
            String versionPackage
    ) throws GitAPIException {
        boolean existsBranch = existsBranch(git, "development", credentialsProvider);
        if (!existsBranch) {
            throw new RuntimeException("No existe la rama development");
        }

        ObjectId branchObjectId = getObjectIdBranch(git, "development");
        if (branchObjectId == null) {
            throw new RuntimeException("No se pudo encontrar el ObjectId de la rama development");
        }

        StringBuilder commitMessage = new StringBuilder("Merge development a " + branch + ". ");

        MergeResult mergeResult = git.merge()
                .setCommit(false)
                .include(branchObjectId).call();
        log(taskListener, "Merge development a " + branch);

        Map<String, int[][]> conflicts = mergeResult.getConflicts();

        if (conflicts != null) {
            boolean[] result = tryToSolveConflicts(conflicts, pomFile, packageFile);
            if (result[0]) {
                String msg = "Se ha solucionado el conflicto de versión en el pom.xml por la versión " + versionPom + " de " + branch + ". ";
                commitMessage.append(msg);
                log(taskListener, msg);
            }
            if (result[1]) {
                String msg = "Se ha solucionado el conflicto de versión en el package.json por la versión " + versionPackage + " de " + branch + ". ";
                commitMessage.append(msg);
                log(taskListener, msg);
            }

        } else {
            boolean isChangedVersionPom = false;
            if (pomFile != null) {
                String otherVersionPom = getVersionPom(pomFile);
                isChangedVersionPom = !versionPom.equals(otherVersionPom);
                log(taskListener, "isChangedVersionPom: " + isChangedVersionPom);
            }
            boolean isChangedVersionPackage = false;
            if (packageFile != null) {
                String otherVersionPackage = getVersionPackage(packageFile);
                isChangedVersionPackage = !versionPackage.equals(otherVersionPackage);
                log(taskListener, "isChangedVersionPackage: " + isChangedVersionPackage);
            }
            if (isChangedVersionPom || isChangedVersionPackage) {
                if (isChangedVersionPom) {
                    boolean updateVersionPom = updateVersionPom(pomFile, versionPom);
                    if (updateVersionPom) {
                        String msg = "Se ha cambiado la versión en el pom.xml del proyecto por la que había en " + branch + " (" + versionPom + "). ";
                        commitMessage.append(msg);
                        log(taskListener, msg);
                    } else {
                        throw new RuntimeException("No se ha podido cambiar la versión en el pom.xml después del merge");
                    }
                }
                if (isChangedVersionPackage) {
                    boolean updateVersionPackage = updateVersionPackage(packageFile, versionPackage);
                    if (updateVersionPackage) {
                        String msg = "Se ha cambiado la versión en el package.json del proyecto por la que había en " + branch + " (" + versionPackage + "). ";
                        commitMessage.append(msg);
                        log(taskListener, msg);
                    } else {
                        throw new RuntimeException("No se ha podido cambiar la versión en el package.json después del merge");
                    }
                }
            } else {
                Status status = git.status().call();
                Set<String> added = status.getAdded();
                Set<String> changed = status.getChanged();
                Set<String> removed = status.getRemoved();
                Set<String> uncommittedChanges = status.getUncommittedChanges();

                if (added.size() > 0 || changed.size() > 0 || removed.size() > 0 || uncommittedChanges.size() > 0) {
                    String msg = "Había cambios sin conflicto";
                    commitMessage.append(msg);
                    log(taskListener, msg);
                } else {
                    log(taskListener, "No había cambios, ni conflictos después del merge.");
                    return;
                }
            }
        }
        add(git);
        commit(git, commitMessage.toString());
        push(git, branch, credentialsProvider);
        log(taskListener, "Se ha hecho commit + push en la rama " + branch);
    }

    protected static ObjectId getObjectIdBranch(Git git, String branch) throws GitAPIException {
        List<Ref> branches = git.branchList().setListMode(ListBranchCommand.ListMode.ALL).call();
        ObjectId branchObjectId = null;
        for (Ref branchRef : branches) {
            String branchName = branchRef.getName();
            if (branchName.contains(branch)) {
                branchObjectId = branchRef.getObjectId();
            }
        }
        return branchObjectId;
    }

    protected static boolean[] tryToSolveConflicts(Map<String, int[][]> conflicts, File pom, File packageJson) {
        boolean[] areSolvedConflicts = new boolean[2];
        String infoConflicts = getInfoConflicts(conflicts);
        if (conflicts.size() > 2) {
            throw new RuntimeException("Hay más de dos ficheros con conflictos:\n" + infoConflicts);
        }

        for (Map.Entry<String, int[][]> conflict : conflicts.entrySet()) {
            boolean isConflictPom = conflict.getKey().toLowerCase().contains("pom.xml");
            boolean isConflictPackage = conflict.getKey().toLowerCase().contains("package.json");

            if (!isConflictPom && !isConflictPackage) {
                throw new RuntimeException("No puede haber un conflicto que no sea en el pom.xml o en el package.json:\n" + infoConflicts);
            }

            if (isConflictPom) {
                int[][] pomConflicts = conflict.getValue();

                if (pom == null) {
                    throw new RuntimeException("No se ha podido encontrar el fichero pom.xml");
                }

                boolean areSolvedPomConflicts = tryToSolvePomConflicts(pomConflicts, infoConflicts, pom);
                if (areSolvedPomConflicts) {
                    areSolvedConflicts[0] = true;
                }
            }

            if (isConflictPackage) {
                int[][] packageConflicts = conflict.getValue();

                if (packageJson == null) {
                    throw new RuntimeException("No se ha podido encontrar el fichero package.json");
                }

                boolean areSolvedPackageConflicts = tryToSolvePackageConflicts(packageConflicts, infoConflicts, packageJson);
                if (areSolvedPackageConflicts) {
                    areSolvedConflicts[1] = true;
                }
            }
        }
        return areSolvedConflicts;
    }

    protected static String getInfoConflicts(Map<String, int[][]> conflicts) {
        StringBuilder info = new StringBuilder();
        for (Map.Entry<String, int[][]> entry : conflicts.entrySet()) {
            String path = entry.getKey();
            info.append(path).append(" ->");
            for (int[] arr : entry.getValue()) {
                int line = arr[1] + 1;
                info.append(" line ").append(line).append(",");
            }
            info = new StringBuilder(info.toString().replaceAll(",$", ";\n"));
        }
        return info.toString();
    }

    protected static boolean tryToSolvePomConflicts(int[][] pomConflicts, String infoConflicts, File pom) {
        if (pomConflicts.length > 1) {
            throw new RuntimeException("Solo puede haber un único conflicto en el pom.xml:\n" + infoConflicts);
        }

        int[] pomConflict = pomConflicts[0];
        int conflictLine = pomConflict[1] + 1;
        try {
            String versionLine = Files.readAllLines(Paths.get(pom.getAbsolutePath())).get(conflictLine);
            if (!versionLine.trim().matches(VERSION_REGEX_POM)) {
                throw new RuntimeException("El conflicto solo puede ser por la versión en el pom.xml:\n" + infoConflicts);
            }

            // Reemplazar el conflicto por version de v12 (versionLine)
            return solveVersionConflict(pom, versionLine);
        } catch (IOException e) {
            throw new RuntimeException("No se ha podido obtener la versión de la línea con conflicto en el pom.xml. IOException : " + e.getMessage());
        }
    }

    protected static boolean tryToSolvePackageConflicts(int[][] packageConflicts, String infoConflicts, File packageJson) {
        if (packageConflicts.length > 1) {
            throw new RuntimeException("Solo puede haber un único conflicto en el package.json:\n" + infoConflicts);
        }

        int[] packageConflict = packageConflicts[0];
        int conflictLine = packageConflict[1] + 1;
        try {
            String versionLine = Files.readAllLines(Paths.get(packageJson.getAbsolutePath())).get(conflictLine);

            // Si el conflicto tiene más de una línea petar, porque significa que tiene más de un conflicto
            areThereConflicts(conflictLine, packageJson);

            if (!versionLine.trim().matches(VERSION_REGEX_PACKAGE)) {
                throw new RuntimeException("El conflicto solo puede ser por la versión en el package.json:\n" + infoConflicts);
            }

            // Reemplazar el conflicto por version de v12 (versionLine)
            return solveVersionConflict(packageJson, versionLine);
        } catch (IOException e) {
            throw new RuntimeException("No se ha podido obtener la versión de la línea con conflicto en el package.json. IOException : " + e.getMessage());
        }
    }

    protected static void areThereConflicts(int conflictLine, File packageJson) {
        int count = 0;
        boolean conflictEnd = false;
        boolean conflictStart = false;
        try {
            List<String> lines = Files.readAllLines(Paths.get(packageJson.getAbsolutePath()));
            for (String line : lines) {
                if (line.trim().startsWith("<<<<<<< HEAD")) {
                    conflictStart = true;
                } else if (line.trim().startsWith("=======")) {
                    conflictEnd = true;
                } else if (conflictStart && !conflictEnd) {
                    count++;
                } else if (conflictEnd) {
                    break;
                }
            }

            if (count > 1) {
                int endConflictLine = conflictLine + count - 1;
                throw new RuntimeException("Hay más de un conflicto en " + packageJson.getName() + ". Desde la línea " + conflictLine + " hasta la línea " + endConflictLine + " incluida.");
            }
        } catch (IOException e) {
            throw new RuntimeException("No se ha podido leer las lineas del fichero " + packageJson.getName() + ". IOException: " + e.getMessage());
        }
    }

    protected static boolean solveVersionConflict(File file, String versionLine) {
        try {
            List<String> newLines = new ArrayList<>();
            boolean conflictEnd = false;
            boolean conflictStart = false;
            List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()));
            for (String line : lines) {
                if (line.trim().startsWith("<<<<<<< HEAD")) {
                    newLines.add(versionLine);
                    conflictStart = true;
                } else if (line.trim().startsWith(">>>>>>>")) {
                    conflictEnd = true;
                } else if (!conflictStart || conflictEnd) {
                    newLines.add(line);
                }
            }

            String collect = String.join("\n", newLines);
            PrintWriter prw = new PrintWriter(file);
            prw.println(collect);
            prw.close();
            return true;

        } catch (FileNotFoundException e) {
            throw new RuntimeException("No se ha podido resolver el conflicto de versión en " + file.getName() + ". FileNotFoundException: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("No se ha podido resolver el conflicto de versión en " + file.getName() + ". IOException: " + e.getMessage());
        }
    }

    protected static int getSecondDigit(String tag) {
        String[] digits = tag.split("\\.");
        if (digits.length <= 1) {
            return -1;
        }
        String secondDigitStr = digits[1];
        try {
            return Integer.parseInt(secondDigitStr);

        } catch (NumberFormatException e) {
            return -1;
        }
    }

    protected static boolean updateVersionPom(File pom, String updateVersion) {
        String pomPath = pom.getAbsolutePath();
        try {
            VTDGen vg = new VTDGen();
            if (!vg.parseFile(pomPath, true)) {
                throw new RuntimeException("No se ha podido parsear el pom.xml");
            }
            VTDNav vn = vg.getNav();
            XMLModifier xm = new XMLModifier(vn);
            AutoPilot ap = new AutoPilot(vn);

            ap.selectXPath("/project/version/text()");
            int i = ap.evalXPath();
            if (i != -1) {
//                String v = vn.toString(i);
                xm.updateToken(i, updateVersion);
                // guardar cambios en el pom
                xm.output(pomPath);
                return true;
            }
            return false;
        } catch (NavException e) {
            throw new RuntimeException("Error NavException en el método updateVersionPom" + e.getMessage());
        } catch (XPathParseException e) {
            throw new RuntimeException("Error XPathParseException en el método updateVersionPom" + e.getMessage());
        } catch (XPathEvalException e) {
            throw new RuntimeException("Error XPathEvalException en el método updateVersionPom" + e.getMessage());
        } catch (ModifyException e) {
            throw new RuntimeException("Error ModifyException en el método updateVersionPom" + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error IOException en el método updateVersionPom" + e.getMessage());
        } catch (TranscodeException e) {
            throw new RuntimeException("Error TranscodeException en el método updateVersionPom" + e.getMessage());
        }
    }

    protected static String toNormalizedStringText(final VTDNav vn) throws NavException {
        int i = vn.getText();
        return vn.toNormalizedString(i);
    }

    protected static boolean updateVersionPackage(File packageJson, String newVersion) {
        try {
            List<String> newLines = new ArrayList<>();
            List<String> lines = Files.readAllLines(Paths.get(packageJson.getAbsolutePath()));
            for (String line : lines) {
                String lineWithoutSpaces = line.replaceAll("\\s", "");

                if (lineWithoutSpaces.trim().startsWith("\"version\"") && line.trim().matches(VERSION_REGEX_PACKAGE)) {
                    String[] split = line.split(":");
                    String oldVersion = split[1].replaceAll("\"", "").replaceAll(",", "").trim();
                    String newLine = line.replace(oldVersion, newVersion);
                    newLines.add(newLine);

                } else {
                    newLines.add(line);
                }
            }
            String collect = String.join("\n", newLines);
            PrintWriter prw = new PrintWriter(packageJson);
            prw.println(collect);
            prw.close();
            return true;

        } catch (FileNotFoundException e) {
            throw new RuntimeException("No se ha podido abrir el archivo " + packageJson.getName() + ". FileNotFoundException: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("No se ha podido leer el archivo " + packageJson.getName() + ". IOException: " + e.getMessage());
        }
    }

    protected static boolean isCompiled(List<GenerateVersionsProject> compiledProjects, String projectName) {
        if (compiledProjects.size() == 0) {
            return false;
        }
        for (GenerateVersionsProject project : compiledProjects) {
            if (project.getProjectName().equals(projectName)) {
                return true;
            }
        }
        return false;
    }

    protected static String getCompiledVersion(List<GenerateVersionsProject> compiledProjects, String projectName) {
        for (GenerateVersionsProject project : compiledProjects) {
            if (project.getProjectName().equals(projectName)) {
                return project.getVersion();
            }
        }
        return null;
    }

//    protected static List<String> getTagsWithoutSnapshot(List<String> tags) {
//        return tags.stream().filter(t -> {
//            if (t.contains("SNAPSHOT")) {
//                return false;
//            }
//
//            return !t.contains("SNAPSHOT");
//
//        }).collect(Collectors.toList());
//    }

    protected static List<String> getTagsWithSameMinorAndRc(List<String> tags, String minor) {
        return tags.stream().filter(t -> {
            String[] digits = t.split("\\.");
            if (digits.length != 3) {
                return false;
            }
            if (!digits[2].contains("RC")) {
                return false;
            }

            return digits[1].equals(minor);

        }).collect(Collectors.toList());
    }

    protected static List<String> getTagsWithEqualOrLesserMinor(List<String> tags, String minor) {
        int minorValue = Integer.parseInt(minor);
        return tags.stream().filter(t -> {
            int secondDigit = getSecondDigit(t);
            if (secondDigit == -1) {
                return false;
            }

            return secondDigit <= minorValue;

        }).collect(Collectors.toList());
    }

    protected static boolean isGreaterThan(String versionStr, String otherVersionStr) {
        Version version = new Version(versionStr.toLowerCase());
        Version otherVersion = new Version(otherVersionStr.toLowerCase());
        int compare = version.compareTo(otherVersion);
        return compare > 0;
    }

    protected static void add(Git git) {
        try {
            git.add().addFilepattern(".").call();
        } catch (GitAPIException e) {
            throw new RuntimeException("No se han podido añadir los cambios. " + e.getMessage());
        }
    }

    protected static void commit(Git git, String msg) {
        try {
            git.commit().setMessage(msg).call();
        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido hacer commit de los cambios. " + e.getMessage());
        }
    }

    protected static void push(Git git, String branch, org.eclipse.jgit.transport.CredentialsProvider credentials) {
        try {
            PushCommand pushCommand = git.push();
            pushCommand.add(branch).setRemote("origin").setCredentialsProvider(credentials).call();
        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido hacer push de los cambios. " + e.getMessage());
        }
    }

    protected static boolean createTag(Git git, String version, org.eclipse.jgit.transport.CredentialsProvider credentials) {
        try {
            // Crear tag en local
            git.tag().setName(version).call();

            // Crear tag en remoto
            git.push().setPushTags().setCredentialsProvider(credentials).call();

            // Comprobar que se ha creado en el remoto y devolver boolean
            return existsBranch(git, version, credentials);
        } catch (GitAPIException e) {
            throw new RuntimeException("No se ha podido crear la tag " + version + ". " + e.getMessage());
        }
    }

    protected static Job<?, ?> getJob(String jobName, TaskListener taskListener) {
        for (Job<?, ?> job : Objects.requireNonNull(Jenkins.getInstanceOrNull()).getAllItems(Job.class)) {
            if (job.getName().equals(jobName)) {
                log(taskListener, "Ha encontrado el job: " + jobName);
                return job;
            }
        }
        return null;
    }

    protected static boolean isJobCompiled(Job<?, ?> job, long maxWait, long startDate, TaskListener taskListener, long sleepWait) {
        try {
            Thread.sleep(sleepWait);
            boolean compiled = false;
            while (true) {
                long nowDate = System.currentTimeMillis();
                long diff = nowDate - startDate;
                if (diff >= maxWait) {
                    break;
                }

                compiled = isBuildCompiled(job, startDate, taskListener);
                log(taskListener, "isCompiled: " + compiled);
                if (compiled) {
                    break;
                }
                Thread.sleep(sleepWait);
            }
            return compiled;
        } catch (InterruptedException e) {
            throw new RuntimeException("El hilo se ha interrumpido en el método isJobCompiled. " + e.getMessage());
        }
    }

    protected static boolean isBuildCompiled(Job<?, ?> job, long startDate, TaskListener taskListener) {
        if (job == null) {
            return false;
        }

        Run<?, ?> lastStableBuild = job.getLastStableBuild();
        if (lastStableBuild != null) {
            long stableTimeInMillis = lastStableBuild.getTimestamp().getTimeInMillis();
            long diff = stableTimeInMillis - startDate;
            log(taskListener, "Estable: " + stableTimeInMillis + " - " + startDate + " = " + diff);
            if (stableTimeInMillis >= startDate) {
                return true;
            }
        }

        Run<?, ?> lastFailedBuild = job.getLastFailedBuild();
        if (lastFailedBuild != null) {
            long failedTimeInMillis = lastFailedBuild.getTimestamp().getTimeInMillis();
            long diff = failedTimeInMillis - startDate;
            log(taskListener, "Fallida: " + failedTimeInMillis + " - " + startDate + " = " + diff);
            if (failedTimeInMillis >= startDate) {
                throw new RuntimeException("La compilación ha terminado con resultado fallido");
            }
        }

        Run<?, ?> lastUnstableBuild = job.getLastUnstableBuild();
        if (lastUnstableBuild != null) {
            long unstableTimeInMillis = lastUnstableBuild.getTimestamp().getTimeInMillis();
            long diff = unstableTimeInMillis - startDate;
            log(taskListener, "Inestable: " + unstableTimeInMillis + " - " + startDate + " = " + diff);
            if (unstableTimeInMillis >= startDate) {
                throw new RuntimeException("La compilación ha terminado con resultado inestable");
            }
        }

        return false;
    }

    protected static void deleteClonedDirectory(String path, TaskListener taskListener) {
        try {
            File deleteDir = new File(path);
            FileUtils.deleteDirectory(deleteDir);
        } catch (IOException e) {
            renameClonedDirectory(path);
            log(taskListener, e.getMessage());
        }
    }

//    protected static void renameClonedDirectory(String path) {
//        File sourceFile = new File(path);
//        String separator = File.separator;
//        String[] split = null;
//        if (separator.equals("\\")) {
//            split = path.split("\\\\");
//        } else {
//            split = path.split("/");
//        }
//
//        String name = split[split.length - 1];
//        String baseDir = path.replaceAll(name, "");
//        String newName = UUID.randomUUID().toString();
//        File destFile = new File(baseDir + newName);
//        sourceFile.renameTo(destFile);
//    }

    protected static void renameClonedDirectory(String path) {
        File sourceFile = new File(path);
        String[] split = path.split(Pattern.quote(File.separator));
        String name = (split.length > 0) ? split[split.length - 1] : path;
        String baseDir = path.replaceAll(name, "");
        String newName = UUID.randomUUID().toString();
        File destFile = new File(baseDir + newName);
        sourceFile.renameTo(destFile);
    }
}
